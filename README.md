# Music Catalog #
**Design and implement a simple online music catalog holding songs and playlist.
**

Required functionalities:

* User registration (and optionally user profiles) / login / logout.
* View all playlists / songs (optionally with paging), without a login
* Listen to songs online. Download songs (optionally).
* Upload songs (optionally validate file size and type), after login.

Optional functionalities:

* Create a new playlist.
* Add comments to songs and playlists.
* Implements genres for the songs and playlists.
* Implement playlists' and songs' ranking system. Show the most highly ranked playlists in a special section at the main page. 
* Functionality for searching / filtering by playlist name / song name / genre.
* Admin panel: add / edit / delete songs, playlists, genres, comments.