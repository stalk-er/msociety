window.parseBoolean = function (string) {

    var bool;

    bool = (function () {
        switch (false) {
            case string.toLowerCase() !== 'true':
                return true;
            case string.toLowerCase() !== 'false':
                return false;
        }
    })();
    if (typeof bool === "boolean") {
        return bool;
    }
    return void 0;
};

window.Player = new jPlayerPlaylist({
    jPlayer: "#jplayer_N",
    cssSelectorAncestor: "#jp_container_N",
    cssSelector: {
        play: "#playerPlay",
        pause: "#playerPause",
        stop: "#stop",
        mute: "#mute",
        unmute: "#unmute",
        currentTime: "#currentTime",
        duration: "#duration",
    }
}, [], {
    playlistOptions: {
        enableRemoveControls: true,
        autoPlay: true,

    },
    swfPath: "js/jPlayer",
    supplied: "webmv, ogv, m4v, oga, mp3",
    smoothPlayBar: true,
    keyEnabled: false,
    audioFullScreen: false,
    volume: (localStorage.getItem('volume') || 0.8),
    muted: (localStorage.getItem('muted')) ? window.parseBoolean(localStorage.getItem('muted')) : false,
    play: function (e) {

        var current_playing = e.jPlayer.status.media;
        $("#emit_current_song").val(JSON.stringify({
            user_id: parseInt($("#current_user").val()),
            playing: current_playing.title
        }));

        $("#emit_current_song").trigger("change");
    }
});

window.Player.containsSong = function (song) {

    var playlist = this.playlist;
    if (playlist.length <= 0)
        return undefined;

    for (var i = 0; i < playlist.length; i++) {

        if (song.title.trim() == playlist[i].title.trim() && (song.artist).toString().trim() === (playlist[i].artist).toString().trim())
            return i;
    }

    return undefined;
};

$(document).on($.jPlayer.event.pause, window.Player.cssSelector.jPlayer, function () {

    $('.musicbar').removeClass('animate');
    $('.jp-play-me').removeClass('active');
    $('.jp-play-me').parent('li').removeClass('active');
    $('.song.playing, .related_song.playing').removeClass("active");
    $('.song.playing .icon-control-play, .related_song.playing .icon-control-play').parent('a').removeClass('active'); // Song is beeing paused
    $('.song.playing, .related_song.playing').removeClass("playing").addClass('paused');
});


$(document).on($.jPlayer.event.play, window.Player.cssSelector.jPlayer, function () {

    $('.musicbar').addClass('animate');
    $('.song.playing, .related_song.playing').addClass("active");
    $('.song.paused .icon-control-play, .related_song.paused .icon-control-play')
            .parent('a').addClass('active');
    $('.song.paused, .related_song.paused .icon-control-play').removeClass("paused").addClass('playing');
});

$(document).on($.jPlayer.event.volumechange, window.Player.cssSelector.jPlayer, function (event) {

    window.Player.options.volume = event.jPlayer.options.volume;
    window.Player.options.muted = event.jPlayer.options.muted;

    localStorage.setItem('muted', event.jPlayer.options.muted);
    localStorage.setItem('volume', event.jPlayer.options.volume);
});

$(document).on('click', '.jp-play-me', function (e) {

    e && e.preventDefault();
    var $this = $(e.target);
    if (!$this.is('a'))
        $this = $this.closest('a');

    $('.jp-play-me').not($this).removeClass('active');
    $('.jp-play-me').parent('li').not($this.parent('li')).removeClass('active');

    $this.toggleClass('active');
    $this.parent('li').toggleClass('active');
    if (!$this.hasClass('active')) {
        Player.pause();
    } else {
        var i = Math.floor(Math.random() * (1 + 7 - 1));
        Player.play(i);
    }
});

$(document).ready(function () {
    var shareDropdown = $("#jp_container_N .share-options");
    $(document).on('click', '.jp-controls .icon-share', function (e) {

        if (window.Player.playlist.length === 0)
            return false;

        var current = window.Player.playlist[window.Player.current];
        if (current) {
            var shareableUrl = window.location.origin + "/listen/song/" + current.url;
            shareDropdown.find(".copy-to-clipboard").val(shareableUrl);
            shareDropdown.find(".icon-share").attr("data-clipboard-text", shareableUrl);
        }
    });
});
