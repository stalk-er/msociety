var played = false;
var is_logged = setTimeout(function () {
    return $("input[type='hidden']#is_logged_js").val();
});

function make_song_active(the_song, className) {

    className = className.replace("playing", "").replace("active", "").split(" ").join("").trim();
    var found = $("body").find("input[type='hidden'][value='" + the_song + "']");
    if (found.length > 0)
    {
        var found_el = found.closest(".song, .related_song");
        $(".icon-control-play").parent().removeClass("active");
        found_el.each(function () {
            found_el_class = $(this).attr("class").replace("playing", "").replace("active", "").split(" ").join("").trim();
            if (found_el_class == className) { // We have a match here


                var item = $(this);
                item.addClass("playing active");
                item.attr("data-played", "true");
                item.find(".icon-control-play")
                .parent()
                .addClass("active");
                var cover = item.find(".song_meta").find("input[name='poster']").val();
                var author = item.find(".song_meta").find("input[name='artist']").val();
                var name = item.find(".song_meta").find("input[name='title']").val();
                $(".song_preview_container").find(".song_preview_cover").attr("src", cover);
                $(".song_preview_container").find("span.song_name").text(name);
                $(".song_preview_container").find("a.song_author").text(author);
            }
            else {
                return;
            }
        });
    }
}
function is_song_playing_from_this_page() {

    var storage = $("#song_playing");
    var page_identifier = $("body").find("#page_identifier").val(); // Equal to page slug
    if (typeof storage.val() == "undefined" || storage.val() == "") // Nothing played from here
    {
        return;
    }
    else { // A song has been played from here
        var the_song = storage.val();
        the_song = JSON.parse(the_song);
        for (var i = 0; i < Player.playlist.length; i++) {
            var s = Player.playlist[i];
            className = the_song.element.replace("playing", "").replace("active", "").split(" ").join("").trim();
            var found = $("body").find("input[type='hidden'][value='" + the_song + "']");
            found = found.closest(".song, .related_song");
            if (found.length > 0)
            found = found.attr("class").replace("playing", "").replace("active", "").split(" ").join("").trim();
            else {
                found = "";
                className = "";
            }


            if (i == Player.current && s.title == the_song.name &&
                className == found
            ) {

                if (page_identifier == "listen") {
                    var has_related = $("#related_html").length;
                    if (has_related > 0) {
                        $("#related_songs").empty();
                        $("#related_songs").append($("#related_html").html());
                        $("#related_songs").show();
                    }
                }


                make_song_active(the_song.name, the_song.element);
            }
        }
    }
}

//function remember_played_song(song_name, el) {
//
//    var page_identifier = $("body").find("#page_identifier").val(); // Equal to page slug
//    var storage = $("#song_playing");
//    if (el.closest(".related_song").length <= 0) // IF it is a related song
//    storage.val("");
//    var data = {
//        name: song_name,
//        element: el.closest(".song, .related_song").attr("class"),
//        identifier: page_identifier
//    };
//    storage.val(JSON.stringify(data)); // Update
//
//    if (page_identifier == "listen") {
//        refresh_related_html();
//    }
//}

//function refresh_related_html() {
//    /**
//    *
//    * Related songs are dynamically loaded,
//    * and on each song play from the right most popular songs side bar,
//    * we load a corresponding related songs to the current playing
//    * and save the related songs as html,
//    * but we have to do it again when song is liked or added to a playlist
//    *
//    * */
//
//
//    setTimeout(function () {
//
//        if (!$("#related_songs").is(":empty")) {
//            var related = $("#related_songs").html();
//            // Save related songs html
//            $("#related_html").empty();
//            $("#related_html").html(related);
//        }
//        else {
//            console.log("It appears that the related records container is empty.");
//        }
//    }, 500);
//}


if (is_logged == 1) { // User is Admin

    function upload_file() {
        if ($("body").find(".modal.fade#upload_now").length > 0) {
            $("body").find(".modal.fade#upload_now").modal("show");
        }

        // Validating the song file extenstions
        $("input[name='song']").change(function (e) {
            var re = /(\.mp3|\.wav|\.ogg)$/i;
            if (!re.exec($(this).val())) {
                $(this).val("");
                $(this).parent().find(".bootstrap-filestyle input[type='text']").val("");
                alert("File type not supported!");
            }
        });
    }
//    function editor_content() {
//        var html = $("#upload_file #editor").html();
//        html = JSON.stringify(html);
//        $("input[name='description']").val(html);
//        return false;
//    }
//
//    function refresh_popover_playlists(playlist) {
//
//        $(".song, .related_song").each(function (e) {
//
//            var sd = parseInt($(this).find("input#song_id").val());
//            if (isNaN(sd) || typeof sd == "undefined")
//            return;
//            var popover_list = $(this).find("i.add_to_playlist").attr("data-content");
//            popover_list_obj = $(popover_list);
//            if (typeof popover_list == "undefined" || popover_list_obj.find("a").text() == "No playlists found.") {
//                popover_list_obj.find("ul").empty();
//            }
//
//            popover_list_obj.find("ul")
//            .append(
//                $("<li>")
//                .append($("<input>").attr("type", "hidden").attr("id", "song_id").val(sd))
//                .append($("<a>").attr("tabindex", "-1").attr("href", "#").attr("onclick", "add_to_playlist(this)").attr("data-playlist-id", playlist.id).text(playlist.name))
//            );
//            var new_html = popover_list_obj.wrap("<p>").parent().html();
//            $(this).find("i.add_to_playlist").attr("data-content", new_html);
//        });
//    }


//    function like_song(item) {
//        // Like
//        item = $(item);
//        var song_id = parseInt( item.closest(".song, .related_song").find("input#song_id").val() );
//        var token = $("head").find("meta[name='token']").attr("content");
//        if (item.closest(".related_song").length > 0) // Yes, this is a related song
//            refresh_related_html();
//
//        Requester.likeSong(song_id, false);
//        Requester.saveToHistory(song_id, false);
//    }
//    function dislike_song(item) {
//        // Dislike
//        item = $(item);
//        var song_id = parseInt(item.closest(".song, .related_song").find("input#song_id").val());
//        var token = $("input[name='_token']").val();
//        if (item.closest(".related_song").length > 0) // Yes, this is a related song
//            refresh_related_html();
//
//        Requester.likeSong(song_id, true);
//        Requester.saveToHistory(song_id, true);
//    }
//    function save_play(item) {
//
//        if (item.closest(".song").data("played") === "true")
//        return false;
//
//        var song_id = parseInt(item.closest(".song, .related_song").find("input#song_id").val());
//        var user_id = parseInt($("#current_user").val());
//
//        var the_play_button = item;
//
//        var song_name = item.closest(".song, .related_song").find('input[name="title"]').val();
//        //push_activity_to_storage(song_id, song_name, true); // Save played song for non logged
//        Requester.playSave(song_id, user_id)
//        .then((data) => {
//            increase_plays(the_play_button);
//            item.closest(".song").data("played", "true");
//        });
//    }
//    function increase_plays(t) {
//        var plays = t.closest(".song").find(".plays");
//        var new_value = parseInt(plays.text().trim()) + 1;
//        plays.text((new_value < 0) ? 0 : new_value);
//    }
//    function load_playlist(playlist, btn) {
//
//        setTimeout(function () { // If no set timeout is set, jQuery cannot find the new created popup from bootstrap
//            btn = $(btn);
//            Requester.loadPlaylist(playlist)
//            .before(() => {
//                btn.attr("disabled", "").text("Loading ...");
//            })
//            .then((data) => {
//                var all_songs = $(".song, .related_song");
//                all_songs.removeClass("active").removeClass("playing").removeClass("paused");
//                all_songs.find(".icon-control-play").parent().removeClass("active");
//                $("#song_playing").val("");
//                $("#related_html").empty();
//                btn.attr("disabled", false).text("Load Playlist"); // Loading animation end
//                data = JSON.parse(data);
//                var songs = prepare_playlist_data(data);
//                Player.remove();
//                Player.stop;
//                Player.setPlaylist(songs);
//                if (typeof Player.playlist[0] !== "undefined") {
//                    Player.play(Player.playlist[0]);
//                    action_msg("alert-success", "The playlist has been loaded !");
//                }
//                else {
//                    action_msg("alert-warning", "Your playlist has been loaded but it is empty.");
//                }
//                return false;
//            });
//        });
//    }
//    function prepare_playlist_data(data) {
//
//        var songs = [];
//        for (var i = 0; i < data.length; i++) {
//            songs.push({
//                mp3: data[i].file,
//                artist: data[i].author,
//                title: data[i].name,
//                poster: data[i].cover,
//            });
//        }
//        return songs;
//    }


//    var objectUrl;
//    $("#hidden_audio").on("canplaythrough", function (e) {
//        var seconds = e.currentTarget.duration;
//        var duration = moment.duration(seconds, "seconds");
//        var time = "";
//        var hours = duration.hours();
//        if (hours > 0) {
//            time = hours + ":";
//        }
//
//        time = time + duration.minutes() + ":" + duration.seconds();
//        console.log(time);
//        $("#song_upload_duration").val(time);
//        URL.revokeObjectURL(objectUrl);
//    });
//
//    $("#upload_file input.filestyle[name='song']").change(function (e) {
//        var file = e.currentTarget.files[0];
//        console.log(file.name);
//        console.log(file.type);
//        console.log(file.size);
//        objectUrl = URL.createObjectURL(file);
//        $("#hidden_audio").prop("src", objectUrl);
//    });

//    $(document).ready(function () {
//        // Calling
//        // Upload avatar preview
//        $("input[type='file'][name='avatar_image']").change(function () {
//            upload_avatar_preview(this);
//        });
//        add_follower();
//        remove_follower();
//        setTimeout(function () {
//            $(".alert-block").fadeOut(function () {
//                $(this).remove();
//            });
//        }, 2000);
//    });
}
else { // User isn't logged

//function push_activity_to_storage(song_id, song_name, played) {
//
//    if (typeof localStorage["user_history"] == "undefined") {
//        var songs = {};
//        songs[song_id] = {
//            song_id: song_id,
//            song_name: song_name,
//            played: played,
//            date_played: new Date()
//        };
//        localStorage.setItem("user_history", JSON.stringify(songs));
//    }
//    else {
//        var songs = localStorage["user_history"];
//        songs = JSON.parse(songs);
//        songs[song_id] = {
//            song_id: song_id,
//            song_name: song_name,
//            played: played,
//            date_played: new Date()
//        };
//        localStorage.setItem("user_history", JSON.stringify(songs));
//    }
//}
}


//function action_msg(alert_type, text) {
//    $("#content")
//    .append(
//        $("<div>").attr("class", "alert " + alert_type).css(
//            {
//                "position": "absolute",
//                "top": 20,
//                "left": "50%",
//                "width": "300px",
//                "transform": "translate(-50%, 0)"
//            }
//        )
//        .append(
//            $("<button>").attr("type", "button").attr("class", "close").attr("data-dismiss", "alert").text(" × ")
//        )
//        .append(
//            $("<p>").text(text)
//        )
//    );
//    setTimeout(function () {
//        $("#content").find("." + alert_type).fadeOut(300, function () {
//            $(this).remove();
//        });
//    }, 2000);
//}

//function update_playlists() {
//
//    var playlists = $("aside#nav").find(".playlists");
//    var playlists_available = playlists.find("li[data-playlist]").map(function () {
//        return parseInt($(this).data("playlist"));
//    }).get();
//
//    Requester.refreshPlaylists(song_id, user_id)
//    .then((playlist)=>{
//        playlist = JSON.parse(playlist);
//        playlist = playlist[0];
//        playlists
//        .append(
//            $("<li>").attr("data-playlist", playlist.id).append(
//                $("<a>").attr("href", "#")
//                .append( $("<i>").attr("class", "icon-playlist icon text-success-lter") )
//                .append( $("<b>").attr("class", "num_of_songs badge bg-success dker pull-right").text(playlist.songs) )
//                .append( $("<span>").text(playlist.name) )));
//                var popup_playlists = $("i.add_to_playlist").attr("data-content");
//            });
//
//        }

//        function already_in_playlist(Player, song) {
//
//            var playlist = Player.playlist;
//            if (playlist.length <= 0)
//            return false;
//            for (var i = 0; i < playlist.length; i++) {
//                if (song.title === playlist[i].title
//                    &&
//                    (song.artist).toString() === (playlist[i].artist).toString()
//                )
//                {
//                    return i;
//                }
//            }
//            return false;
//        }

        function insert_into_player(Player, item) {

            var song = {};
            var meta = item.closest(".song, .related_song").find(".song_meta");
            if (meta.length <= 0) {
                action_msg("alert-warning", "The song cannot be played because the song meta is missing. Please contact the administrator.");
                return false;
            }

            if (is_logged == 1) {
                var meta = item.closest(".song, .related_song").attr("data-played", "true");
            }
            meta.find("input[type='hidden']").each(function () { // Song meta
                song[$(this).attr("name")] = $(this).val();
            });
            if (item.closest(".song, .related_song").hasClass("paused")) {
                setTimeout(function () {
                    $("#jplayer_N").jPlayer("play");
                }, 200);
            }
            else {
                setTimeout(function () {
                    $("#jplayer_N").jPlayer("stop");
                    Player.setPlaylist([song]);
                    $("#jplayer_N").jPlayer("play");
                }, 200);
                remember_played_song(song.title, item);
                /**
                * Save the song play to the database
                */
                if (does_function_exists(save_play)) {
                    save_play(item);
                }
            }
            $(".song, .related_song")
            .removeClass("playing")
            .removeClass("paused")
            .removeClass("active");
            $(".song .icon-control-play, .related_song .icon-control-play")
            .parent()
            .removeClass("active");
            item.closest(".song, .related_song").addClass("playing active"); // Identifier for a currently playing song
            item.find(".icon-control-play").parent().addClass("active");
        }

//        function fill_preview_data(data) {
//            /**
//            * Generating the song preview
//            */
//            $(".song_preview_cover").attr("src", data.cover).attr("alt", data.name);
//            $(".song_name").text(data.name);
//            // $(".song_desc").text(data.description);
//            $(".song_author").text(data.author.name)
//            .attr("href", window.location.origin + "/user/" + data.author.id)
//            .attr("data-bjax", "")
//            .attr("data-replace", window.location.origin + "/user/" + data.author.id)
//            .attr("data-url", window.location.origin + "/user/" + data.author.id)
//            .attr("data-target", ".to_put")
//            .attr("data-el", "#bjax-target");
//        }
        
        // TBC
//        function load_song_preview(item, Player) {
//
//            /**
//            * Checking if this element exists on the page,
//            * because we need to execute the following function only on the 'Listen' page
//            */
//            var related = $("#related_songs");
//            if (related.length <= 0)
//            return;
//            related.removeAttr("hidden");
//            related.empty();
//            // Data to send over POST
//            var author = item.closest(".song").find("input[type='hidden'][name='song_author']").val();
//            var song_id = item.closest(".song").find("input[type='hidden']#song_id").val();
//            var token = $("head").find("meta[name='token']").attr("content");
//            /**
//            * Let's take the related songs to the
//            * current song by genres
//            * and display them
//            */
//            $.ajax({
//                type: "post",
//                url: window.location.origin + "/get/song",
//                data: {
//                    author: author,
//                    song_id: song_id,
//                    _token: token
//                },
//                success: function (data) {
//                    data = JSON.parse(data);
//                    fill_preview_data(data);
//                    /**
//                    * Putting the related songs in place
//                    * and preparing them for playing in the player
//                    */
//                    var related_song;
//                    var related = $("#related_songs");
//                    for (var i in data.related) {
//                        var song = data.related[i];
//                        related_song = $("#song_preview").find("li.related_song.template").clone();
//                        related_song.css({
//                            "display": "block"
//                        });
//                        related_song.removeClass("template");
//                        related_song.addClass("sub-item-" + i);
//                        related_song.find(".related_song_name").text(song.name);
//                        related_song.find(".song_meta input[name='mp3']").val(song.file);
//                        related_song.find(".song_meta input[name='artist']").val(song.author);
//                        related_song.find(".song_meta input[name='title']").val(song.name);
//                        related_song.find(".song_meta input[name='poster']").val(song.cover);
//                        related_song.append($("<input>").attr("type", "hidden").attr("id", "song_id").val(song.id));
//                        related_song.append($("<input>").attr("type", "hidden").attr("id", "author_id").val(song.user_id));
//                        if (song.likes)
//                        related_song.find(".fa-heart-o").parent().addClass("active");
//                        // Append to the container
//                        related.append(related_song);
//                    }
//                    setTimeout(function () {
//                        refresh_related_songs_playlists();
//                    }, 1000);
//                }
//            });
//        }
        
        // TBC
//        function refresh_related_songs_playlists() {
//            
//            var url_playlists = window.location.origin + "/get/user/playlists";
//            $.ajax({
//                url: url_playlists,
//                type: "get",
//                data: {},
//                success: function (data) {
//                    data = JSON.parse(data);
//                    $(".related_song").each(function (e) {
//
//                        var sd = parseInt($(this).find("input#song_id").val());
//                        if (isNaN(sd) || typeof sd == "undefined")
//                        return;
//                        var popover_list = $(this).find("i.add_to_playlist").attr("data-content");
//                        popover_list_obj = $(popover_list);
//                        popover_list_obj.empty();
//                        for (var i in data) {
//                            var playlist = data[i];
//                            popover_list_obj
//                            .append(
//                                $("<li>")
//                                .append(
//                                    $("<input>")
//                                    .attr("type", "hidden")
//                                    .attr("id", "song_id")
//                                    .val(sd)
//                                )
//                                .append(
//                                    $("<a>")
//                                    .attr("tabindex", "-1")
//                                    .attr("href", "#")
//                                    .attr("onclick", "add_to_playlist(this)")
//                                    .attr("data-playlist-id", playlist.id)
//                                    .text(playlist.name)
//                                )
//                            );
//                            var pl_songs = [];
//                            for (var x in playlist.playlist_songs) {
//                                var song = playlist.playlist_songs[x];
//                                pl_songs.push(parseInt(song.id));
//                            }
//
//
//                            if ($.inArray(sd, pl_songs) !== -1) {
//                                console.log(playlist.id);
//                                popover_list_obj.find("li").last()
//                                .append($("<input>").attr("type", "hidden").attr("name", "in_playlist"))
//                                .find("a").attr("onclick", "remove_from_playlist(this)")
//                                .append($("<i>").attr("class", "fa fa-check pull-right"));
//                            }
//
//                        }
//                        var new_html = popover_list_obj.wrap("<p>").parent().html();
//                        $(this).find("i.add_to_playlist").attr("data-content", new_html);
//                    });
//                }
//            });
//        }

        function does_function_exists(function_name) {
        
            if (typeof function_name !== 'undefined' && $.isFunction(function_name)) {
                return true;
            }
            return false;
        }
//        
//        function upload_avatar_preview(input) {
//
//            if (input.files && input.files[0]) {
//                var reader = new FileReader();
//                reader.onload = function (e) {
//                    var src = e.target.result;
//                    $(".avatar_image").attr("src", src);
//                };
//                reader.readAsDataURL(input.files[0]);
//            }
//        }

//        function add_follower() {
//
//            $("#follow").click(function () {
//                var follow = $(this);
//                follow.attr("id", "unfollow");
//                follow.unbind("click");
//                remove_follower();
//                var user_to_follow = Number( $("#user_page_id").val() );
//                var follower = Number( $("#current_user_id").val() );
//                Requester.followUser(user_to_follow, follower);
//            });
//        }

//        function remove_follower() {
//
//            $("#unfollow").click(function () {
//                var unfollow = $(this);
//                unfollow.attr("id", "follow");
//                unfollow.unbind("click");
//                add_follower();
//                var user_to_follow = parseInt($("#user_page_id").val());
//                var follower = parseInt($("#current_user_id").val());
//                Requester.unfollowUser(user_to_follow, follower);
//            });
//        }

        function page_refresh(item) {

            window.location = window.location.href;
        }

//        function see_notification(item) {
//
//            item = $(item);
//            if (item.data("seen") == "seen")
//            return false;
//
//            var not_id = Number(item.closest(".media").find("#notification_id").val());
//
//            Requester.seeNotification()
//            .then((not_id) => {
//                data = JSON.parse(data);
//                var html = '<small class="media-footer block m-b-none">SEEN <i class="fa fa-check pull-right"></i></small>';
//                item.data("seen", 'seen');
//
//                item.closest(".media").append($(html));
//                var not_count = parseInt($("#notifications .notification_count").text());
//
//                if (!isNaN(not_count)) {
//                    not_count -= 1;
//                    $(".notification_count").text(not_count);
//                }
//            });
//        }
