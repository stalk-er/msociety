function Application()
{
    var that = this; // Object global

    this.user = function () {

        var user = null;
        var isLogged = Number($('#is_logged_js').val());

        if (isLogged == 1) // It's logged
        {

            user = {userID: $('#current_user').val()};

            // Follow another user
            $(document).on("click", "#follow", function () {

                var follow = $(this);
                follow.attr("id", "unfollow");
                //follow.unbind("click");
                ////remove_follower();
                var user_to_follow = Number($("#user_page_id").val());
                var follower = Number($("#current_user_id").val());
                Requester.followUser(user_to_follow, follower);
            });

            // Unfollow another user
            $(document).on("click", "#unfollow", function () {

                var unfollow = $(this);
                unfollow.attr("id", "follow");
                //unfollow.unbind("click");
                //add_follower();
                var user_to_follow = parseInt($("#user_page_id").val());
                var follower = parseInt($("#current_user_id").val());
                Requester.unfollowUser(user_to_follow, follower);
            });

        }

        return user;
    };  // This is the current user that is using the application

    this.seeNotification = function (item) {

        item = $(item);
        if (item.data("seen") == "seen")
            return false;

        var not_id = Number(item.closest(".media").find("#notification_id").val());

        Requester.seeNotification(not_id)
                .then((data) => {
                    data = JSON.parse(data);
                    var html = '<small class="media-footer block m-b-none">SEEN <i class="fa fa-check pull-right"></i></small>';
                    item.data("seen", 'seen');

                    item.closest(".media").append($(html));
                    var not_count = parseInt($("#notifications .notification_count").text());

                    if (!isNaN(not_count)) {
                        not_count -= 1;
                        $(".notification_count").text(not_count);
                    }
                });
    };

    this.ajaxHandle = function () {

        $(document).on({
            ajaxStart: function () {
            },
            ajaxStop: function () {
            },
            ajaxError: function (data) {

                alert(data);
                console.log(data);
            }
        });
    }; // This is some global events for all elements

    this.handlePopOvers = function () {

        $('[data-toggle="popover"]').popover();
        $(document).on("click", ".add_to_playlist", function (e) {
            $(".add_to_playlist").not(this).popover('hide');
        });

        $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

        $(".scrollable").on('scroll', function (e) {
            $('[data-toggle="popover"]').each(function () {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
    };

    this.genreActive = function () {

        $(document).on("click", ".genres_list a.list-group-item", function (e) {

            $("body .genres_list").find("a.list-group-item").removeClass("active");
            $(this).addClass("active");
        });
    };

    // Populate the non logged user history a.k.a temp history
    this.populateTempHistory = function () {

        if ($("table[data-template='history_item']").length > 0)
        {

            var data;
            if (typeof localStorage["user_history"] !== "undefined")
                data = JSON.parse(localStorage["user_history"]);

            var url = window.location.origin + "/local-history";
            var token = $("head").find("meta[name='token']").attr("content");
            $.ajax({
                url: url,
                type: "post",
                data: {
                    _token: token,
                    songs: data
                },
                success: function (songs) {
                    songs = JSON.parse(songs);

                    for (var i in songs) {
                        var template = $("body").find("table[data-template='history_item']").find("tr.history_item").clone();

                        template.addClass("song");
                        template.find("td:nth-child(1)").text(songs[i].name);
                        template.find("td:nth-child(1)").append($("<input>").attr("type", "hidden").attr("id", "song_id").val(songs[i].id));
                        template.find("td:nth-child(1)")
                                .append(
                                        $("<span>").attr("class", "song_meta").attr("hidden", true)
                                        .append($("<input>").attr("type", "hidden").attr("name", "mp3").val(songs[i].file))
                                        .append($("<input>").attr("type", "hidden").attr("name", "artist").val(songs[i].author))
                                        .append($("<input>").attr("type", "hidden").attr("name", "title").val(songs[i].name))
                                        .append($("<input>").attr("type", "hidden").attr("name", "poster").val(songs[i].cover))
                                        );

                        template.find("td:nth-child(1)").append($("<i>").attr("class", "icon-control-play").attr("style", "font-size: 12px; cursor: pointer; margin-left: 10px;"));
                        template.find("td:nth-child(2)").text(songs[i].last_played);
                        $("body").find("table.activity_history tbody").append(template);
                    }
                }
            });
        }
    };

    this.searchAutocomplete = function () {

        var innerThat = this;

        this.highlightText = function (text, $node) {

            if (text == " ")
                return;

            var searchText = $.trim(text).toLowerCase(), currentNode = $node.get(0).firstChild, matchIndex, newTextNode, newSpanNode;
            while ((matchIndex = currentNode.data.toLowerCase().indexOf(searchText)) >= 0) {
                newTextNode = currentNode.splitText(matchIndex);
                currentNode = newTextNode.splitText(searchText.length);
                newSpanNode = document.createElement("span");
                newSpanNode.className = "highlight";
                currentNode.parentNode.insertBefore(newSpanNode, currentNode);
                newSpanNode.appendChild(newTextNode);
            }
        };

        $("#primary-search").autocomplete({
            type: "get",
            source: window.location.origin + "/autocomplete",
            minLength: 1,
            focus: function (event, ui) {
                $(this).val(ui.item.name);
                return false;
            },
            select: function (event, ui) {

                var item = ui.item;

                if (item.type == "song") {

                    var song = {
                        mp3: item.mp3,
                        artist: item.artist,
                        title: item.title,
                        poster: item.poster
                    };

                    SongManager.play(song);
                    SongManager.savePlay(undefined, {
                        songId: item.id,
                        songName: item.title
                    });
                }
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {

            var $a = $("<a></a>").text(item.label).css({
                "width": "100%"
            });

            if (item.type == "user")
                $a.attr("href", "/user/" + item.hidden_value)
                        .attr("data-target", ".to_put")
                        .attr("data-el", "#bjax-target")
                        .attr("data-url", "/user/" + item.hidden_value)
                        .attr("data-replace", "/user/" + item.hidden_value)
                        .attr("data-bjax", true)

            if (item.type == "genre")
                $a.attr("href", "/genres/" + item.hidden_value)
                        .attr("data-target", ".to_put")
                        .attr("data-el", "#bjax-target")
                        .attr("data-url", "/genres/" + item.hidden_value)
                        .attr("data-replace", "/genres/" + item.hidden_value)
                        .attr("data-bjax", true)



            innerThat.highlightText(this.term, $a);

            return $("<li class='song'>").data("ui-autocomplete-item", item)
                    .append(
                            $a.append($("<span>").text(" | ").append($("<strong>").text(item.type)))
                            )
                    .appendTo(ul);
        };
    };

    this.fbShareSong = function (url) {
        window.open(url, "Share to facebook", "status = 1, height = 500, width = 360, resizable = 0");
    };

    this.songUrlCopyToClipboard = function () {

        var clipboard = new Clipboard('.song-to-clipboard');

        clipboard.on('success', function (e) {
            $(e.trigger).closest('.popover').find('input').effect('highlight');
            NotificationManager.pushNotification("The url was copied to clipboard", "success");
            e.clearSelection();
        });

        clipboard.on('error', function (e) {
            
            NotificationManager.pushNotification("An error occured while copying to your clipboard. Try to refresh the page", "error", 5000);
            console.error('Action:', e.action);
            console.error('Trigger:', e.trigger);
        });

    };

    this.start = function () {

        this.handlePopOvers();
        this.ajaxHandle();

        $(document).on("click", ".find-banner", function (e) {
            e.preventDefault();

            $("body").find("#primary-search").focus();
        });

        // Make genre active when clicking among the genres
        this.genreActive();

        // Calculate song duration when user upload a song
        var objectUrl;
        $("#hidden_audio").on("canplaythrough", function (e) {

            var seconds = e.currentTarget.duration;
            var duration = moment.duration(seconds, "seconds");
            var time = "";
            var hours = duration.hours();
            if (hours > 0) {
                time = hours + ":";
            }

            time = time + duration.minutes() + ":" + duration.seconds();
            console.log(time);
            $("#song_upload_duration").val(time);
            URL.revokeObjectURL(objectUrl);
        });

        $(document).on("change", "#upload_file input.filestyle[name='song']", function (e) {

            var file = e.currentTarget.files[0];
            console.log(file.name);
            console.log(file.type);
            console.log(file.size);
            objectUrl = URL.createObjectURL(file);
            $("#hidden_audio").prop("src", objectUrl);
        });

        // Uploading user avatar and preview during upload
        // Calling
        // Upload avatar preview
        $(document).on("change", "input[type='file'][name='avatar_image']", function (e) {

            var input = $(e.target)[0];
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var src = e.target.result;
                    $(".avatar_image").attr("src", src);
                };
                reader.readAsDataURL(input.files[0]);
            }
        });

        if (that.user() == null)
            this.populateTempHistory();

        this.searchAutocomplete();

        this.songUrlCopyToClipboard();

        $(document).on('click', '.song-share-fb', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            that.fbShareSong(url);
        });

        this.jQueryChosenInit();

    };

    this.jQueryChosenInit = function () {

        var select, chosen;

        // cache the select element as we'll be using it a few times
        select = $(".chosen-select");
        // init the chosen plugin
        select.chosen({no_results_text: 'Press Enter to add new entry:'});

        // get the chosen object
        chosen = select.data('chosen');

        // Bind the keyup event to the search box input
        chosen.search_field.on('keyup', function (e)
        {
            // if we hit Enter and the results list is empty (no matches) add the option
            if (e.which == 13 && chosen.dropdown.find('li.no-results').length > 0)
            {
                Requester.createGenre({
                    genre: {
                        name: $(e.target).val(),
                        description: ""
                    }
                }).done(function (data) {

                    var option = $("<option>").val(data.genre.id).text(data.genre.name);
                    // add the new option
                    select.prepend(option);
                    // automatically select it
                    select.find(option).prop('selected', true);
                    // trigger the update
                    select.trigger("chosen:updated");

                }).error(function (e) {
                    alert("Error creating the genre. Please try again. If this happen again, report to the administrator.");
                });
            }
        });
    };

    return this;
}
