function User() {

    this.seeNotification = function (item) {
        item = $(item);
        if (item.data("seen") == "seen")
            return false;

        var not_id = Number(item.closest(".media").find("#notification_id").val());

        Requester.seeNotification()
                .then(function (not_id) {
                    data = JSON.parse(data);
                    var html = '<small class="media-footer block m-b-none">SEEN <i class="fa fa-check pull-right"></i></small>';
                    item.data("seen", 'seen');

                    item.closest(".media").append($(html));
                    var not_count = parseInt($("#notifications .notification_count").text());

                    if (!isNaN(not_count)) {
                        not_count -= 1;
                        $(".notification_count").text(not_count);
                    }
                });
    };

  


}