//var songs = [];
//// Init
//function play_sub_song(item) {
//    item = $(item);
//
//    var container = item.closest(".related_song");
//    var preview_data = container.find(".song_meta");
//
//
//    insert_into_player(Player, item);
//
//    var preview_data = {
//        name: preview_data.find("input[name='title']").val(),
//        cover: preview_data.find("input[name='poster']").val(),
//        description: container.find("input[type='hidden']#song_desc").val(),
//        author: {
//            id: container.find("input[type='hidden']#author_id").val(),
//            name: preview_data.find("input[name='artist']").val()
//        }
//    };
//
//    fill_preview_data(preview_data);
//}
//
//function pause_song(item) {
//    item = $(item);
//
//
//
//    if (item.closest(".song, .related_song").hasClass("playing")) {
//        item.closest(".song, .related_song").removeClass("playing").addClass("paused"); // Identify paused song
//        console.log("Pausing the song now.");
//        setTimeout(function () {
//            Player.pause();
//        }, 200);
//    }
//
//}
//function add_song(item) {
//
//    item = $(item);
//
//    if (is_logged == 0) { // If user not logged
//        var song_id = Number( item.closest(".song").find("input#song_id").val() );
//        push_activity_to_storage(song_id);
//    }
//
//    if ($("#page_identifier").val() == "listen" && item.closest(".song").length > 0) {
//        load_song_preview(item, Player);
//
//
//    }
//
//
//    insert_into_player(Player, item);
//}
//
//console.log("player init");
//
//var Player = new jPlayerPlaylist({
//    jPlayer: "#jplayer_N",
//    cssSelectorAncestor: "#jp_container_N",
//    cssSelector: {
//        play: ".icon-control-play",
//        pause: ".icon-control-pause",
//        stop: "#stop",
//        mute: "#mute",
//        unmute: "#unmute",
//        currentTime: "#currentTime",
//        duration: "#duration"
//    }
//},
//songs,
//        {
//            playlistOptions: {
//                enableRemoveControls: true,
//                autoPlay: true
//            },
//            swfPath: "js/jPlayer",
//            supplied: "webmv, ogv, m4v, oga, mp3",
//            smoothPlayBar: true,
//            keyEnabled: false,
//            audioFullScreen: false,
//            play: function (e) {
//                var current_playing = e.jPlayer.status.media;
//                $("#emit_current_song").val(JSON.stringify({
//                    user_id: parseInt($("#current_user").val()),
//                    playing: current_playing.title
//                }));
//                $("#emit_current_song").trigger("change");
//            }
//        });
//
//
//$(document).on($.jPlayer.event.pause, Player.cssSelector.jPlayer, function () {
//    $('.musicbar').removeClass('animate');
//    $('.jp-play-me').removeClass('active');
//    $('.jp-play-me').parent('li').removeClass('active');
//
//    $('.song.playing, .related_song.playing').removeClass("active");
//
//    $('.song.playing .icon-control-play, .related_song.playing .icon-control-play').parent('a').removeClass('active'); // Song is beeing paused
//    $('.song.playing, .related_song.playing').removeClass("playing").addClass('paused');
//});
//
//$(document).on($.jPlayer.event.play, Player.cssSelector.jPlayer, function () {
//    $('.musicbar').addClass('animate');
//
//    $('.song.playing, .related_song.playing').addClass("active");
//
//    $('.song.paused .icon-control-play, .related_song.paused .icon-control-play')
//            .parent('a').addClass('active');
//    $('.song.paused, .related_song.paused .icon-control-play').removeClass("paused").addClass('playing');
//
//
//});
//$(document).on('click', '.jp-play-me', function (e) {
//    e && e.preventDefault();
//    var $this = $(e.target);
//    if (!$this.is('a'))
//        $this = $this.closest('a');
//
//    $('.jp-play-me').not($this).removeClass('active');
//    $('.jp-play-me').parent('li').not($this.parent('li')).removeClass('active');
//
//    $this.toggleClass('active');
//    $this.parent('li').toggleClass('active');
//    if (!$this.hasClass('active')) {
//        Player.pause();
//    } else {
//        var i = Math.floor(Math.random() * (1 + 7 - 1));
//        Player.play(i);
//    }
//});
//
//// Play song, pause song
////if (does_function_exists(load_playlist))
////    load_playlist(Player); // Load a playlist
//
//primary_search_autocomplete(Player);
//function primary_search_autocomplete(Player) {
//
//    function highlightText(text, $node) {
//        if (text == " ")
//            return;
//        var searchText = $.trim(text).toLowerCase(), currentNode = $node.get(0).firstChild, matchIndex, newTextNode, newSpanNode;
//        while ((matchIndex = currentNode.data.toLowerCase().indexOf(searchText)) >= 0) {
//            newTextNode = currentNode.splitText(matchIndex);
//            currentNode = newTextNode.splitText(searchText.length);
//            newSpanNode = document.createElement("span");
//            newSpanNode.className = "highlight";
//            currentNode.parentNode.insertBefore(newSpanNode, currentNode);
//            newSpanNode.appendChild(newTextNode);
//        }
//    }
//
//    $("#primary-search").autocomplete({
//        type: "get",
//        source: window.location.origin + "/autocomplete",
//        minLength: 1,
//        focus: function (event, ui) {
//            $(this).val(ui.item.name);
//            return false;
//        },
//        select: function (event, ui) {
//            var item = ui.item;
//            if (item.type == "song") {
//                console.log(item);
//
//                var song = {
//                    mp3: item.mp3,
//                    artist: item.artist,
//                    title: item.title,
//                    poster: item.poster
//                };
//
//                // Load the song into the player if it is not there or find it and play it
//                var already_in = already_in_playlist(Player, song);
//                if (already_in !== false) { // If the song already exists in the player, get it's index and play it
//                    Player.pause();
//
//                    //Avoid the Promise Error
//                    setTimeout(function () {
//                        Player.play(already_in);
//                    }, 500);
//                } else { // If it's not in the player, add it and play it
//                    Player.add(song);
//                    Player.pause();
//
//                    //Avoid the Promise Error
//                    setTimeout(function () {
//                        Player.play((Player.playlist.length) - 1);
//                    }, 500);
//                }
//            }
//            if (item.type == "genre") {
//                window.location = window.location.origin + "/genres/" + item.hidden_value;
//            }
//            if (item.type == "user") {
//                window.location = window.location.origin + "/user/" + item.hidden_value;
//            }
//        }
//    }).data("ui-autocomplete")._renderItem = function (ul, item) {
//        var $a = $("<a></a>").text(item.label);
//        console.log(this.term);
//        highlightText(this.term, $a);
//        return $("<li>")
//                .data("ui-autocomplete-item", item)
//                .append($("<li>").append($a).append($("<span>").text(" | ").append($("<strong>").text(item.type))))
//                .appendTo(ul);
//    };
//}
