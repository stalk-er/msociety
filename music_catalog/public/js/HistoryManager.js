function HistoryManager() {

    this.pushSong = function (song_id, song_name, played) {

        if (typeof localStorage["user_history"] == "undefined") {

            var songs = {};
            songs[song_id] = {
                song_id: song_id,
                song_name: song_name,
                played: played,
                date_played: new Date()
            };

            localStorage.setItem("user_history", JSON.stringify(songs));
        } else {

            var songs = localStorage["user_history"];
            songs = JSON.parse(songs);
            songs[song_id] = {
                song_id: song_id,
                song_name: song_name,
                played: played,
                date_played: new Date()
            };

            localStorage.setItem("user_history", JSON.stringify(songs));
        }
    };
    
    this.init = (function () {
        

    })();

    return this;

}