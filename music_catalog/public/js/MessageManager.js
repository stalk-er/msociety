var MessageManager = (function () {

    this.newMessage = function (alert_type, text) {

        $("#content").append($("<div>").attr("class", "alert " + alert_type)
                .css({
                    "position": "absolute",
                    "top": 20,
                    "left": "50%",
                    "width": "300px",
                    "transform": "translate(-50%, 0)"
                })
                .append($("<button>").attr("type", "button").attr("class", "close").attr("data-dismiss", "alert").text(" × "))
                .append($("<p>").text(text))
                );

        window.setTimeout(function () {

            $("#content").find("." + alert_type).fadeOut(300, function () {
                $(this).remove();
            });
        }, 2000);
    };

})();