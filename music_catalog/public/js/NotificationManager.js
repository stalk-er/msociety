function NotificationManager() {

    this.pushNotification = function (text, type, stayTime) {
        
        var dismissAfter = 3 * 1000;
        if(stayTime) dismissAfter = stayTime;
        
        var html = $(`
        <div class="alert-container col-md-6">
            <div class="alert alert-`+type+`">
                <button type="button" class="close" data-dismiss="alert">×</button>
                `+text+`
            </div>
        </div>
        `);
        
        $('body').prepend(html);
        setTimeout(function () {
            html.fadeOut(function () {
                $(this).remove();
            });
        }, dismissAfter);
    };

    return this;
}