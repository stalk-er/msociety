function PlaylistManager() {

    var that = this;

    this.playlists = $("ul.playlists");

    this.updateAll = function () {

        Requester.refreshPlaylists().then(function (playlist) {
            playlist = JSON.parse(playlist);
            playlist = playlist[0];

            that.refreshPopOverDropdowns(playlist, 'add'); // Refresh dropdown playlists

            var popover_markup =
                    "<div class='load_playlist_popover' data-playlist='" + playlist.id + "' style='height: auto; width: auto; z - index: 9999; padding: 10px;'>" +
                    "<h4 class='text-center'>Are you sure you want to load this playlist now ?</h4>" +
                    //"<button type='button' class='btn btn-success btn-lg load_now'>Load Playlist</button>" +
                    "<button type='button' class='btn btn-success btn-md loadNow'>Load Playlist</button> " +
                    "<button type='button' class='btn btn-danger btn-md deleteNow'>Delete Playlist</button>" +
                    "</div>";

            var markup = '<li data-playlist="' + playlist.id + '"><a style="cursor: pointer;" data-toggle="popover" title data-container="#content" data-html="true" data-placement="right" data-content="' + popover_markup + '" data-original-title="' + playlist.name + '" data-animation="true">' +
                    '<i class="icon-playlist icon text-success-lter"></i>' +
                    '<b class="badge bg-success dker pull-right num_of_songs">0</b>' +
                    '<span>' + playlist.name + '</span>' +
                    '</a></li>';

            that.playlists.append(markup);
        });
    };

    this.refreshPopOverDropdowns = function (playlist, command) {

        /**
         * @argument {object} playlist 
         * @argument {int} playlist 
         * @argument {string} command 
         * 
         * @description The playlist argument returns a javascript object when the command is `add`
         * and returns int when the command passed is `delete`
         * */


        $(".song, .related_song").each(function (e) {

            var sd = parseInt($(this).find("input#song_id").val());
            if (isNaN(sd) || typeof sd == "undefined")
                return;

            var popover_list = $(this).find("i.add_to_playlist").attr("data-content");
            popover_list_obj = $(popover_list);

            switch (command) {

                case "add":

                    if (typeof popover_list == "undefined" || popover_list_obj.find("a").text() == "No playlists found.") {
                        popover_list_obj.find("ul").empty();
                    }
                    popover_list_obj.find("ul").append(
                            $("<li>").append($("<input>").attr("type", "hidden").attr("id", "song_id").val(sd))
                            .append(
                                    $("<a>").attr("tabindex", "-1")
                                    .attr("href", "#")
                                    .attr("id", "addToPlaylist")
                                    .attr("data-playlist-id", playlist.id)
                                    .text(playlist.name))
                            );

                    break;

                case "delete":
                    popover_list_obj.find('[data-playlist-id="' + playlist + '"]').closest("li").remove();
                    break;
            }

            var noPlaylistsFound = "<li><a>No playlists found.</a></li>";
            if (popover_list_obj.find("ul li").length === 0) {
                popover_list_obj.find("ul").append(noPlaylistsFound);
            }
            var newHtml = popover_list_obj.wrap("<p>").parent().html();

            $(this).find("i.add_to_playlist").attr("data-content", newHtml);
        });


    };

    this.create = function (e) {
        e.preventDefault();

        var elementClicked = $(e.target);
        var name = elementClicked.closest("form").find("input[name='name']").val();
        var description = elementClicked.closest("form").find("input[name='description']").val();

        if (Application.user)
            userID = Application.user().userID;

        Requester.createPlaylist(name, description, userID).then(function (x) {
            $(".create_playlist").popover("hide");
            that.updateAll();
            NotificationManager.pushNotification("Successfully created a playlist", "success");
        });
    };

    this.loadPlaylist = function (e) {

        setTimeout(function () {
            // If no set timeout is set, jQuery cannot find the new created popup from bootstrap

            btn = $(e.target);
            var playlist = btn.closest(".load_playlist_popover").data("playlist");
            var beforeSend = function () {
                btn.attr("disabled", true);
            };
            Requester.loadPlaylist(playlist, beforeSend)
                    .then(function (data) {
                        data = JSON.parse(data);


                        var all_songs = $(".song, .related_song");
                        all_songs.removeClass("active").removeClass("playing").removeClass("paused");
                        all_songs.find(".icon-control-play").parent().removeClass("active");
                        btn.attr("disabled", false); // Loading animation end

                        var songs = [];
                        if (data.length > 0)
                            for (var i = 0; i < data.length; i++) {
                                songs.push({
                                    mp3: data[i].file,
                                    artist: data[i].author,
                                    title: data[i].name,
                                    poster: data[i].cover,
                                    url: data[i].url
                                });
                            }
                        else {
                            NotificationManager.pushNotification("The playlist is empty. You can easily add songs to your playlists.", "info");
                        }
                        window.Player.remove();
                        window.Player.stop;
                        window.Player.setPlaylist(songs);

                        if (typeof Player.playlist[0] !== "undefined") {
                            window.Player.play(window.Player.playlist[0]);
                        } else {
                            NotificationManager.pushNotification("Successfully loaded the playlist", "success");
                        }


                        return false;
                    });
        }, 10);
    };

    this.deletePlaylist = function (e) {

        setTimeout(function () {
            // If no set timeout is set, jQuery cannot find the new created popup from bootstrap

            btn = $(e.target);
            var playlist = btn.closest(".load_playlist_popover").data("playlist");
            Requester.deletePlaylist(playlist).then(function (data) {
                $('[data-toggle="popover"]').popover('hide');
                that.playlists.find('[data-playlist="' + playlist + '"]').remove();
                that.refreshPopOverDropdowns(playlist, 'delete');
                NotificationManager.pushNotification("The selected playlist was deleted!", "warning");
                return false;
            });
        }, 10);
    };

    this.addSong = function (e) {

        var elementClicked = $(e.target);
        var playlistID = Number(elementClicked.data("playlist-id"));
        var songID = Number(elementClicked.parent().find("input[type='hidden']#song_id").val());
        if (elementClicked.attr('id') == 'addToPlaylist') // Adding to playlist
        {
            Requester.saveToPlaylist(playlistID, songID, Number(Application.user().userID), function (e) {
                elementClicked.append($("<i>").attr("class", "icon-reload pull-right rotate360")); // Loading animation
                elementClicked.unbind("click");


            })
                    .then(function (data) {
                        //action_msg("alert-success", "The song was added to the playlist.");
                        elementClicked.attr('id', 'removeFromPlaylist')
                        elementClicked.find(".icon-reload").detach(); // Remove loading
                        elementClicked.append($("<i>").attr("class", "fa fa-check pull-right")); // Checked icon
                        elementClicked.parent("li").append($("<input>").attr("name", "in_playlist").attr("type", "hidden"));
                        that.increaseSongs(playlistID);
                        var new_content = $(elementClicked.closest("div.popover-content")).html();
                        $("input[type='hidden'][value='" + songID + "']").closest(".song, .related_song").find("i.add_to_playlist").attr("data-content", new_content);

                        NotificationManager.pushNotification("The song was added to your playlist", "success");
                    });
        }
    };

    this.removeSong = function (e) {

        var elementClicked = $(e.target);
        var playlistID = Number(elementClicked.data("playlist-id"));
        var songID = Number(elementClicked.parent().find("input[type='hidden']#song_id").val());

        // Checking if the particular song is in playlist
        if (elementClicked.attr('id') == 'removeFromPlaylist') // Remove from playlist
        {
            Requester.removeFromPlaylist(playlistID, songID, function () {
                elementClicked.find(".fa.fa-check").detach();
                elementClicked.append($("<i>").attr("class", "icon-reload pull-right rotate360"));
                elementClicked.unbind('click');
            })
                    .then(function () {
                        elementClicked.find(".icon-reload").detach(); // Remove loading icon
                        elementClicked.parent("li").find("input[name='in_playlist']").detach();
                        elementClicked.attr('id', 'addToPlaylist');
                        that.decreaseSongs(playlistID);
                        //action_msg("alert-success", "The song was removed from the playlist");
                        var new_content = $(elementClicked.closest("div.popover-content")).html();
                        $("input[type='hidden'][value='" + songID + "']").closest(".song, .related_song").find("i.add_to_playlist").attr("data-content", new_content);

                        NotificationManager.pushNotification("The song was successfully removed from this playlist", "warning");
                    });
        }
    };

    this.increaseSongs = function (playlistID) {
        var playlist = $("aside#nav .playlists").find("li[data-playlist=" + playlistID + "]");
        var new_value = parseInt(playlist.find(".num_of_songs").text().trim()) + 1;
        playlist.find(".num_of_songs").text((new_value < 0) ? 0 : new_value);
    };

    this.decreaseSongs = function (playlistID) {
        var playlist = $("aside#nav .playlists").find("li[data-playlist=" + playlistID + "]");
        var new_value = parseInt(playlist.find(".num_of_songs").text().trim()) - 1;
        playlist.find(".num_of_songs").text((new_value < 0) ? 0 : new_value);
    };

    this.init = function () {

        $(document).on("click", "#createPlaylist", this.create);
        $(document).on("click", "#removeFromPlaylist", this.removeSong);
        $(document).on("click", "#addToPlaylist", this.addSong);
        $(document).on("click", ".loadNow", this.loadPlaylist);
        $(document).on("click", ".deleteNow", this.deletePlaylist);
    };

    return this;
}
