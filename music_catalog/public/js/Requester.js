var Requester = (function () {

    var baseUrl = window.location.origin + '/';
    var token = $('head').find('meta#token').attr('content');
    function _getHeaders()
    {
        var header = {};
        var token = $('head meta[name="token"]').attr('content');
        if (token)
            header['X-CSRF-TOKEN'] = $('head').find('meta#token').attr('content');
        return header;
    }
    function _makeRequest(method, url, data, before)
    {
        return $.ajax({
            method: method,
            url: baseUrl + url,
            headers: _getHeaders(),
            data: data,
            beforeSend: before
        });
    }
    function seeNotification(not_id)
    {
        return _makeRequest('post', 'notification/seen', {
            id: not_id
        });
    }
    function loadPlaylist(playlist_id, beforeSend)
    {
        return _makeRequest('get', 'load/playlist/' + playlist_id, {}, beforeSend);
    }

    function deletePlaylist(playlist_id, beforeSend)
    {
        return _makeRequest('post', 'delete/playlist/' + playlist_id, {}, beforeSend);
    }
    function playlistsRefresh()
    {
        return _makeRequest('get', 'playlists/refresh', {});
    }
    function userFollow(user_to_follow, follower)
    {
        return _makeRequest('post', 'follow', {
            user_id: user_to_follow,
            followed_by: follower,
        });
    }
    function userUnfollow(user_to_follow, follower)
    {

        return _makeRequest('post', 'unfollow', {
            user_id: user_to_follow,
            followed_by: follower,
        });
    }
    function savePlay(song_id, user_id)
    {
        return _makeRequest('post', 'song/play', {
            song_id: song_id,
            user_id: user_id
        });
    }
    function songLike(song_id, active)
    {
        return _makeRequest('post', 'song/like', {
            song_id: song_id,
            active: active
        });
    }
    function songDislike(song_id, active)
    {
        return _makeRequest('post', 'song/like', {
            song_id: song_id,
            active: active
        });
    }
    function saveToHistory(song_id, active)
    {
        return _makeRequest('post', 'history/save', {
            song_id: song_id,
            active: active
        });
    }

    function createPlaylist(name, description, user_id)
    {
        return _makeRequest('post', 'create/playlist', {
            name: name,
            description: description,
            user_id: user_id
        });
    }

    function removeFromPlaylist(playlist_id, song_id, before)
    {
        return _makeRequest('post', 'song/out-of/playlist', {
            playlist_id: playlist_id,
            song_id: song_id
        }, before);
    }

    function saveToPlaylist(playlist_id, song_id, user_id, before)
    {
        return _makeRequest('post', 'song/to/playlist', {
            playlist_id: playlist_id,
            song_id: song_id,
            user_id: user_id
        }, before);
    }

    function getSong(id) {
        var url = window.location.origin + "/song/" + id + "/get";
        return _makeRequest("GET", url, {}, null);
    }

    function createGenre(data) {
        
        return _makeRequest('post', '/genres/create', {
            genre: data
        });
    }

    return {
        seeNotification: seeNotification,
        createGenre: createGenre,
        loadPlaylist: loadPlaylist,
        deletePlaylist: deletePlaylist,
        refreshPlaylists: playlistsRefresh,
        followUser: userFollow,
        unfollowUser: userUnfollow,
        playSave: savePlay,
        likeSong: songLike,
        dislikeSong: songDislike,
        saveToHistory: saveToHistory,
        createPlaylist: createPlaylist,
        removeFromPlaylist: removeFromPlaylist,
        saveToPlaylist: saveToPlaylist,
        getSong: getSong,
    };
})();
