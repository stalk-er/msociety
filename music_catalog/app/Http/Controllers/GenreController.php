<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class GenreController extends Controller {

    public function get_genres() {
        return view("admin.genres")
                        ->with("songs", \App\Song::paginate(10));
    }

    public function get_genre_songs_ajax(Request $request) {

        $id = intval($request["genre_id"]);

        $genre = DB::table('genres')->where("id", $id)->first();

        $genre_songs = DB::table('song_genre')->where('genre_id', $id)->get();
        $ids = [];
        for ($i = 0; $i < count($genre_songs); $i++) {
            $ids[$i] = $genre_songs[$i]->id;
        }

        $genre_songs = DB::table("songs")->whereIn("id", $ids)->get();

        return view("admin.genre")->with("single_genre", $genre)->with("genre_songs", $genre_songs);
    }

    public function get_genre_songs($id) {
        $genre = \App\Genre::find($id);
        $genre_songs = \App\Genre::find($id)->songs()->paginate(10);

        return view("admin.genre")
                        ->with("single_genre", $genre)
                        ->with("genre_songs", $genre_songs);
    }

    public function create_genre(Request $request) {

        $genre = $request['genre']['genre'];

        if (\App\Genre::where('name', '=', $genre['name'])->exists()) {
            return response()->json([
                        "msg" => "The same genre already exists",
                        "type" => "warn"
            ]);
        }

        if ($newGenre = \App\Genre::create($genre)) {
            return response()->json([
                        "msg" => "A new genre was created",
                        "type" => "success",
                        "genre" => $newGenre
            ]);
        }

        abort(500);
    }

}
