<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class HistoryController extends Controller {

    public function generate_stat(Request $request) {

        $item_id = intval($request["history_item"]);
        return json_encode($item_id);
    }

    public function local_storage_history(Request $request) {
        $songs = $request["songs"];
        $filtered = [];
        $count = 0;
        $name;

        if ($songs) {
            foreach ($songs as $s => $song) {

                $song_data = \App\Song::find(intval($song["song_id"]));

                if (null === $song_data) // If such song does not exists
                    continue;

                $filtered[$count] = [
                    "id" => $song_data->id,
                    "file" => $song_data->file,
                    "cover" => $song_data->cover,
                    "name" => $song_data->name,
                    "author" => $song_data->author,
                    "last_played" => date('F d, Y - g:i', strtotime('+3 hours', strtotime($song["date_played"]))),
                ];
                $count++;
            }
            foreach ($filtered as $k => $v) {
                $dates[$k] = $v["last_played"];
            }
            array_multisort($dates, SORT_DESC, $filtered);
    }



        return json_encode($filtered);
    }

}
