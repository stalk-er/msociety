<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class PlaylistController extends Controller {

    public function create_playlist(Request $request) {

        $playlist = new \App\Playlist();
        $playlist->name = $request["name"];
        $playlist->description = $request["description"];
        $playlist->user_id = $request["user_id"];
        $playlist->save();
    }

    public function get_last_playlist() {
        $playlist = Auth::user()->playlists()->orderBy('created_at', 'desc')->take(1)->get();

        $playlist->first()->songs = count($playlist->first()->songs());
        return json_encode($playlist);
    }

    public function load_playlist($id) {

        $playlist = \App\Playlist::find($id);
        return json_encode($playlist->songs());
    }
    
    public function delete_playlist($id) {

        $playlist = \App\Playlist::find($id);
        if($playlist->delete()) {
            return response()->json([
                "msg"   => "Successfully delete the playlist.",
                "type"  => "success"
            ]);
        }
        abort(500);
    }
}
