<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Auth;
use Storage;
use File;
use DB;
use Response;
use Crypt;

class Songster extends Controller {

    public function post_delete(Request $request) {

        $song_id = (int) $request["song_id"];
        $song = \App\Song::find($song_id);
        $dir = preg_split('~[\\\\/]~', $song->file);
        File::deleteDirectory(public_path("/songs/" . $dir[2]));
        $song->genres()->detach();
        $song->playlists()->detach();
        $song->delete();
    }

    public function post_upload(Request $request) {

        if ($request->hasFile('cover') && $request->hasFile('song')) {

            $song = $request->file('song');
            $cover = $request->file('cover');
            $relative_path = "/songs/" . pathinfo($request->file('song')->getClientOriginalName(), PATHINFO_FILENAME);
            $absolute_path = public_path() . "/songs/" . pathinfo($request->file('song')->getClientOriginalName(), PATHINFO_FILENAME);

            if (File::makeDirectory($absolute_path, $mode = 0777, true, true)) {
                if ($song->move($absolute_path, $song->getClientOriginalName()) &&
                        $cover->move($absolute_path, $cover->getClientOriginalName())
                ) {

                    $song = new \App\Song();
                    $song->name = Input::get("name");
                    $song->description = Input::get("description");
                    $song->author = Auth::user()->name;
                    $song->plays = 0;
                    $song->duration = Input::get("song_size");
                    $song->likes = 0;
                    $song->rating = 0;
                    $song->file = $relative_path . "/" . $request->file('song')->getClientOriginalName();
                    $song->cover = $relative_path . "/" . $request->file('cover')->getClientOriginalName();
                    $song->user_id = Auth::user()->id;
                    $song->save();
                    $song->url = md5($song->id . '-' . $song->created_at);
                    $song->save();
                    $song->genres()->attach(Input::get("genres"));

                    $activity = new \App\Activity();
                    $activity->user_id = Auth::user()->id;
                    $activity->activity_content = "<strong>" . Auth::user()->name . "</strong> uploaded a new song called <strong>" . $song->name . "</strong>";
                    $activity->save();
                } else {
                    return redirect("/")->with("warning", "The file couldn't be moved due to file size limit or system error.");
                }
            }
            return redirect("/")->with("success", "The song has been uploaded successfully!");
        }
    }

    public function like_song(Request $request) {

        if (!$request["song_id"] || $request["song_id"] == "NaN")
            return;

        if ($request["active"] == "true") {

            $song = \App\Song::find(intval($request["song_id"]));
            $song->likes -= 1;
            if ($song->likes < 0)
                $song->likes = 0;
            $song->save();
        } else {

            $song = \App\Song::find(intval($request["song_id"]));
            $song->likes += 1;
            if ($song->likes < 0)
                $song->likes = 0;
            $song->save();
        }
    }

    public function save_in_history(Request $request) {

        $sid = intval($request["song_id"]); // The song ID
        if (!$request["song_id"] || $request["song_id"] == "NaN")
            return;

        if ($request["active"] == "false") {
            $history = \App\UserHistory::firstOrCreate([
                        "user_id" => Auth::user()->id,
                        "song_id" => $sid,
            ]);
            $history->liked = 1;
            $history->save();
        } else {
            $history = \App\UserHistory::firstOrCreate([
                        "user_id" => Auth::user()->id,
                        "song_id" => $sid,
            ]);
            $history->liked = 0;
            $history->save();
        }
    }

    public function get_song(Request $request) {

        $author = $request["author"];
        $song_id = intval($request["song_id"]);
        $song = \App\Song::find($song_id);

        $related = [];
        $genres = $song->genres()->get();

        foreach ($genres as $genre) {
            array_push($related, $genre->songs()->get());
        }

        $result = array();
        foreach ($related as $inner) {
            $result[key($inner)] = current($inner);
        }

        $related = [];
        foreach ($result as $inner) {

            foreach ($inner as $in) {
                if ($in->id == $song_id) {
                    unset($in);
                    continue;
                }
                array_push($related, $in);
            }
        }

        if (Auth::user())
            foreach ($related as $rel) {
                if (Auth::user()->likes($rel))
                    $rel->likes = true;
                else
                    $rel->likes = false;
            }



        $user = DB::table("users")->where("name", "LIKE", $author)->first();
        $song->author = $user;
        $song->related = $related;

        $data = $song;
        return json_encode($data);
    }

    function getSong($songUid) {
        $song = \App\Song::where("url", "=", $songUid);
        if ($song->exists()) {
            return view("admin.listen")->with('singleSong', $song->first());
        }
        abort(404);
    }

    public function add_to_playlist(Request $request) {

        $playlist = \App\Playlist::find(intval($request["playlist_id"]));
        $sid = intval($request["song_id"]);
        $song = \App\Song::find($sid);
        if ($song) {
            $song->playlist()->save($playlist);
            $uid = intval($request["user_id"]);
            $user = \App\User::find($uid);
            $activity = new \App\Activity();
            $activity->user_id = $user->id;
            $activity->activity_content = "<strong>$user->name</strong> added <strong>$song->name</strong> to his playlist <strong>$playlist->name</strong>";
            $activity->save();
        }
    }

    public function remove_from_playlist(Request $request) {
        //$playlists = $this->playlists();

        $playlist = \App\Playlist::find(intval($request["playlist_id"]));
        $song = \App\Song::find(intval($request["song_id"]));

        $song->playlist()->detach($playlist);
    }

    public function song_play(Request $request) {

        $song_id = intval($request["song_id"]);

        $song = \App\Song::find($song_id);
        $song->plays += 1;
        $song->save();

        $uid = intval($request["user_id"]);
        $log = \App\UserHistory::where("song_id", "=", $song_id)->where("user_id", "=", $uid)->first();



        if (null !== $log) {
            $log->plays += 1;
            $log->save();
        } else {

            $new_log = new \App\UserHistory();
            $new_log->song_id = $song_id;
            $new_log->user_id = $uid;
            $new_log->liked = 0;
            $new_log->plays += 1;
            $new_log->save();
        }
    }

    public function get_upload_history() {

        $user_id = Auth::user()->id;
        $user_uploads = DB::table("songs")->where("user_id", "=", $user_id)->orderBy("updated_at", "DESC")->get();
        return view("admin.user_uploads")->with("user_uploads", $user_uploads);
    }

    public function edit_song_name(Request $request) {
        $id = intval($request["song_id"]);
        $song = \App\Song::find($id);
        $song->name = $request["song_name"];
        $song->save();
    }

}
