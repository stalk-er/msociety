<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class ActivityController extends Controller {

    public function follow_me(Request $request) {

        $user_id = intval($request["user_id"]);
        $followed_by = intval($request["followed_by"]);
        if ($user_id == $followed_by)
            return;

        $new_relation = DB::table("followers")->insert([
            "user_id" => $user_id,
            "followed_by" => $followed_by
        ]);

        $activity = new \App\Activity();
        $activity->user_id = $followed_by;
        $activity->activity_content = "<strong>" . \App\User::find($followed_by)->name . "</strong> just followed <strong>" . \App\User::find($user_id)->name . "</strong>";
        $activity->save();

        $activity = new \App\Activity();
        $activity->user_id = $user_id;
        $activity->activity_content = "<strong>" . \App\User::find($user_id)->name . "</strong> have been followed by <strong>" . \App\User::find($followed_by)->name . "</strong>";
        $activity->save();

        $user = \App\UserMeta::where("user_id", "=", $user_id)->first();

        if (null !== $user) {
            $user->followers += 1;
            $user->save();
        } else {
            $user = new \App\UserMeta();
            $user->user_id = $user_id;
            $user->followers = 1;
            $user->following = 0;
            $user->fb = null;
            $user->tw = null;
            $user->gp = null;
            $user->save();
        }
        $has_followings = \App\UserMeta::where("user_id", "=", $followed_by)->first();
        $has_followings->following += 1;
        $has_followings->save();
    }

    public function unfollow_me(Request $request) {

        $user_id = intval($request["user_id"]);
        $followed_by = intval($request["followed_by"]);
        $new_relation = DB::table("followers")
                ->where("user_id", "=", $user_id)
                ->where("followed_by", "=", $followed_by)
                ->delete();

        $activity = new \App\Activity();
        $activity->user_id = $followed_by;
        $activity->activity_content = "<strong>" . \App\User::find($followed_by)->name . "</strong> just unfollowed <strong>" . \App\User::find($user_id)->name . "</strong>";
        $activity->save();

        $activity = new \App\Activity();
        $activity->user_id = $user_id;
        $activity->activity_content = "<strong>" . \App\User::find($user_id)->name . "</strong> have been unfollowed by <strong>" . \App\User::find($followed_by)->name . "</strong>";
        $activity->save();

        $user = \App\UserMeta::where("user_id", "=", $user_id)->first();
        $user->followers -= 1;
        $user->fb = null;
        $user->tw = null;
        $user->gp = null;
        $user->save();

        $has_followings = \App\UserMeta::where("user_id", "=", $followed_by)->first();
        $has_followings->following -= 1;
        $has_followings->save();
    }

    public function seen(Request $request) {

        $id = (int) $request["id"];
        $activity = \App\Activity::find($id);
        $activity->seen = true;
        if ($activity->save()) {
            return "true";
        }
        return "false";
    }

}
