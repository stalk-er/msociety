<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Input;

class SearchController extends Controller {

    public function autocomplete(Request $request) {
        $term = $request["term"];
//        $term = Input::get('term', false);
//        dd($term);

        $songs = \App\Song::where("name", "LIKE", "%$term%")->get();
        $users = \App\User::where("name", "LIKE", "%$term%")->get();
        $genres = \App\Genre::where("name", "LIKE", "%$term%")->get();

        $su = $songs->merge($users);
        $sug = $su->merge($genres);

        $type = "";
        for ($i = 0; $i < count($sug); $i++) {

            if (get_class($sug[$i]) === "App\Song") {
                $type = "song";

                $sug[$i]->mp3 = $sug[$i]->file;
                $sug[$i]->artist = $sug[$i]->author;
                $sug[$i]->title = $sug[$i]->name;
                $sug[$i]->poster = $sug[$i]->cover;
                
            } elseif (get_class($sug[$i]) === "App\User") {
                $type = "user";
            } elseif (get_class($sug[$i]) === "App\Genre") {
                $type = "genre";
            }


            $sug[$i]->type = $type;
            $sug[$i]->value = $sug[$i]->name;
            $sug[$i]->label = $sug[$i]->name;
            $sug[$i]->hidden_value = $sug[$i]->id;
        }
        return response()->json($sug);
    }

    public function render_results() {
        
    }

}
