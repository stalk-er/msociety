<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Hash;
use File;
use DB;

class UserController extends Controller {

    public function get_history() {

        $data = [];
        if (Auth::user()) {
            $user = Auth::user();
            $data = $user->get_history()->orderBy("updated_at", "DESC")->get();
        }
        return view("admin.activity_history")->with("history", $data);
    }

    public function get_settings() {

        return view("admin.user_settings");
    }

    public function post_settings(Request $request) {

        $user = Auth::user();

        $user->update(array_filter($request->input()));
                //dd($request->hasFile('avatar_image'));

        if ($request->hasFile('avatar_image')) {
            $image = $request->file('avatar_image');
            $file_name = str_replace([",", '"', "-", " ", "^", "%"], "_", strtolower($image->getClientOriginalName()));
            $user_dir = str_replace([",", '"', "-", " ", "^", "%"], "_", strtolower($user->name));
            $absolute_path = public_path() . "/avatars/" . $user_dir;

            if (File::makeDirectory($absolute_path, $mode = 0777, true, true)) {

                if ($image->move($absolute_path, $file_name)) {
                    $user->avatar_image = "/avatars/$user_dir/" . $file_name;
                    $user->save();
                } else {
                    return redirect("/profile-settings")->with("warning", "The file couldn't be moved due to file size limit or system error.");
                }
            } else {
                File::cleanDirectory($absolute_path);
                if ($image->move($absolute_path, $file_name)) {
                    $user->avatar_image = "/avatars/$user_dir/" . $file_name;
                    $user->save();
                } else {
                    return redirect("/profile-settings")->with("warning", "The file couldn't be moved due to file size limit or system error.");
                }
            }
        }

        $user_meta = \App\UserMeta::where("user_id", "=", $user->id)->first();
       
        if (null == $user_meta) {

            $user_meta = new \App\UserMeta();
            $user_meta->user_id = $user->id;
            $user_meta->info = $request->input("info");
            $user_meta->fb = $request->input("fb");
            $user_meta->tw = $request->input("tw");
            $user_meta->gp = $request->input("gp");
            $user_meta->save();
        } else {
            $user_meta->user_id = $user->id;
            $user_meta->info = $request->input("info");
            $user_meta->fb = $request->input("fb");
            $user_meta->tw = $request->input("tw");
            $user_meta->gp = $request->input("gp");
            $user_meta->save();
             
        }

       // return redirect("/profile-settings")->with("success", "Settings successfuly updated.");
    }

    public function get_profile($id) {

        $user_meta = DB::table('users_meta')->where('user_id', '=', $id)->first();
        $user = \App\User::find($id);

        $user_type = \App\UserType::find($user->type)->name;
        //$user_uploads = DB::table("songs")->where("user_id", "=", $id)->orderBy("updated_at", "DESC")->get();
        $user_uploads = $user->songs()->orderBy("updated_at", "DESC")->get();

        if ($user_meta !== null) :
            return view("admin.profile")
                            ->with("user", $user)
                            ->with("meta", $user_meta)
                            ->with("user_type", $user_type)
                            ->with("user_uploads", $user_uploads);
        else :
            return view("admin.profile")
                            ->with("user", \App\User::find($id))
                            ->with("user_type", $user_type)
                            ->with("user_uploads", $user_uploads);
        endif;
    }

    public function get_user_playlists() {

        $ups = Auth::user()->get_playlists_with_songs();
        return json_encode($ups);
    }

}
