<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


Route::auth();

/** Pages */
Route::get('/', 'HomeController@index');
//Route::get('/dashboard', 'HomeController@index');

/** Upload Modal */
Route::get('/upload-modal', function() {
    return view("modals.upload");
});


Route::get('/activities', 'ActivityController@get_new_activities');
Route::post('/upload', 'Songster@post_upload');
Route::post('/delete/song', 'Songster@post_delete');
Route::get('/all-songs', 'Songster@get_songs');
Route::post('/create/playlist', 'PlaylistController@create_playlist');
Route::get('/playlists/refresh', 'PlaylistController@get_last_playlist');
Route::get('/get/user/playlists/', 'UserController@get_user_playlists');
Route::post('/song/to/playlist', 'Songster@add_to_playlist');
Route::post('/song/out-of/playlist', 'Songster@remove_from_playlist');
Route::get('/load/playlist/{id}', 'PlaylistController@load_playlist');
Route::post('/delete/playlist/{id}', 'PlaylistController@delete_playlist');

Route::group(['middleware' => 'web'], function () {

    // Put all your routes inside here.
    Route::post('/song/name/edit', 'Songster@edit_song_name');
    Route::post('/song/like', 'Songster@like_song');
    Route::post('/history/save', 'Songster@save_in_history');
    Route::post('/song/play', 'Songster@song_play');
});


Route::get('/genres', "GenreController@get_genres");
Route::get('/genres/{id}', "GenreController@get_genre_songs");
Route::post('/genres/create', "GenreController@create_genre");

Route::get('/listen', function() {
    return view("admin.listen");
});
Route::get('/profile', function() {
    return view("admin.profile");
});
Route::get('/profile-settings', 'UserController@get_settings');
Route::post('/save-settings', 'UserController@post_settings');
Route::get('/history', 'UserController@get_history');

Route::post('/get/song', 'Songster@get_song');

// Get song by ID
Route::get('/listen/song/{any}', 'Songster@getSong');

Route::post('/generate-statistics', 'HistoryController@generate_stat');
Route::post('/local-history', 'HistoryController@local_storage_history');

Route::get('/uploads', 'Songster@get_upload_history');
Route::get('/user/{id}/uploads', 'Songster@get_user_uploads');

Route::get('/user/{id}', 'UserController@get_profile');

Route::post('/follow', 'ActivityController@follow_me');
Route::post('/unfollow', 'ActivityController@unfollow_me');

Route::get('/autocomplete', 'SearchController@autocomplete');

// Download Song
Route::get('/songs/{id}/download', function($request) {
    $id = $request;
    $song = \App\Song::find($id);
    $file = public_path() . $song->file;
    return Response::download($file);
});


Route::post('/notification/seen', 'ActivityController@seen');



Route::get('/chat', function() {
    return view('chat');
});
Route::get('/chat-me', 'ChatController@send');

Route::get('/socket', 'SocketController@index');
Route::post('/sendmessage', 'SocketController@sendMessage');
Route::get('/writemessage', 'SocketController@writemessage');
