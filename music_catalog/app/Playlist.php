<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model {

    protected $table = "playlists";
    protected $fillable = [
        "name",
        "description",
        "user_id",
    ];

    public function songs() {
        return $this->belongsToMany("\App\Song", "playlist_song", "playlist_id", "song_id")->get();
    }

    

    public function contains($song) {
        return $this->songs()->contains($song);
    }

}
