<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model {

    protected $table = "users_meta";
    protected $fillable = [
        "user_id",
        "info",
        "fb",
        "tw",
        "gp",
        "followers",
        "following",
    ];

}
