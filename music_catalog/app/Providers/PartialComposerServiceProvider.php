<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Auth;

class PartialComposerServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

        $this->upload_data();
        $this->top_songs_data();
        $this->new_songs_data();
        $this->new_users_data();
        $this->side_menu_data();
        $this->genres_sidebar_data();
        $this->most_popular_sidebar_data();
        $this->featured_songs_data();
        $this->members_sidebar_data();
        $this->user_activity_data();
        $this->user_last_listens();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    public function upload_data() {

        return view()->composer("modals.upload", function($view) {
                    $view->with('genres', \App\Genre::all());
                });
    }

    public function members_sidebar_data() {
        return view()->composer("modals.members_sidebar", function($view) {
                    $current_user = Auth::user();
                    $followed_by_me = DB::table("followers")->where("followed_by", "=", $current_user->id)->pluck("user_id");
                    $users = \App\User::findMany($followed_by_me);

                    $view->with('users', $users);
                });
    }

    public function new_songs_data() {

        return view()->composer("modals.new_songs", function($view) {
                    $new = DB::table('songs')
                            ->orderBy('updated_at', 'desc')
                            ->take(10)
                            ->get();
                    $view->with('new_songs', $new);
                });
    }

    public function new_users_data() {
        return view()->composer("modals.new_users", function($view) {
                    $new = DB::table('users')
                            ->orderBy('created_at', 'desc')
                            ->take(10)
                            ->get();
                    $view->with('new_users', $new);
                });
    }

    public function top_songs_data() {

        return view()->composer("modals.top_songs", function($view) {
                    $top = DB::table('songs')
                            ->orderBy('plays', 'desc')
                            ->take(10)
                            ->get();
                    $view->with('top_songs', $top);
                });
    }

    public function side_menu_data() {

        return view()->composer("modals.side_menu", function($view) {
                    if (Auth::check()) {
                        $view->with('playlists', Auth::user()->playlists()->get());
                    } else {
                        $view->with('playlists', []);
                    }
                });
    }

    public function genres_sidebar_data() {

        return view()->composer("modals.genres_sidebar", function($view) {
                    $view->with('genres', \App\Genre::all());
                });
    }

    public function most_popular_sidebar_data() {

        return view()->composer("modals.most_popular_side", function($view) {
                    //$most_popular = \App\Song::orderBy("plays", "desc")->take(10)->get();
                    $toExclude = [];
                    if(Auth::user())
                    {
                        $toExclude = Auth::user()->get_history()->get()->pluck("id");
                    }
                    else
                        $toExclude = [];
                    
                    $most_popular = \App\Song::orderBy("plays", "desc")
                                    ->whereNotIn('id', $toExclude)
                                    ->take(10)->get();
                    $view->with('most_popular', $most_popular);
                });
    }

    public function featured_songs_data() {

        return view()->composer("modals.featuredSongs", function($view) {

                    if (Auth::user()) {
                        $featuredSongs = Auth::user()->get_history()->get();
                        $view->with('featuredSongs', $featuredSongs);
                    } else {
                        $featuredSongs = \App\Song::orderBy("likes")->orderBy("plays")->get();
                        $view->with('featuredSongs', $featuredSongs);
                    }
                });
    }

    public function user_activity_data() {

        return view()->composer("modals.user_dashboard", function($view) {
                    $user_id = $view->user->id;
                    $activities = \App\Activity::where("user_id", "=", $user_id)->orderBy("created_at", "desc")->get();
                    $view->with('activities', $activities);
                });
    }

    public function user_last_listens() {

        return view()->composer("modals.user_preferences", function($view) {

                    $last_listened = \App\UserHistory::where("user_id", $view->user->id)
                            ->orderBy("created_at", "desc")
                            ->pluck("song_id")
                            ->take(5);

                    $last_listened = \App\Song::findMany($last_listened);

                    $view
                            ->with('last_listened', $last_listened);
                });
    }

}
