<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'avatar_image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

   

    public function likes($song) {

        $liked = DB::table('user_history')->where("user_id", $this->id)->where("song_id", $song->id)->first();
        if (isset($liked)) {
            $liked = ($liked->liked == 1) ? true : false;
            return $liked;
        }
        return false;
    }

    public function songs() {
        return $this->hasMany("\App\Song");
    }

    public function playlists() {

        return $this->belongsTo(("\App\Playlist"), "id", "user_id");
    }

    public function get_history() {

        return $this->belongsToMany("\App\Song", "user_history", "user_id", "song_id");
    }

    public function meta() {

        return $this->hasOne("\App\UserMeta");
    }

    public function is_followed_by($id) {

        return DB::table("followers")->where("user_id", "=", $this->id)->where("followed_by", $id)->first() !== null;
    }

    public function hierarchy_type() {
        return $this->belongsTo("\App\UserType", "type", "id")->first();
    }

    public function get_playlists_with_songs() {

        $user_id = $this->id;
        $user_playlists = $this->playlists()->get();

        $data = [];
        foreach ($user_playlists as $playlist) {

            $pl_s = $playlist->songs();

            $playlist->playlist_songs = $pl_s;
            array_push($data, $playlist);
        }

        return $data;
    }
}
