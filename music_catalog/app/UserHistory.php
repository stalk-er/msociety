<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHistory extends Model
{
    protected $table = "user_history";
    protected $fillable = [
        "user_id",
        "song_id",
        "liked",
        "plays"
    ];
}
