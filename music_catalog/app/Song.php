<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Crypt;

class Song extends Model {

    protected $table = "songs";
    protected $fillable = [
        "name",
        "description",
        "author",
        "genre",
        "plays",
        "likes",
        "rating",
        "file",
        "cover",
        "user_id",
        "url",
    ];

    public static function boot() {
        parent::boot();

        // cause a delete of a product to cascade to children so they are also deleted
        static::deleted(function($song) {
            $song->genres()->detach();
            $song->playlists()->detach();
        });
    }

    public function genres() {
        $table = "song_genre";
        return $this->belongsToMany("\App\Genre", $table, "song_id", "genre_id");
    }

    public function playlist() {
        return $this->belongsToMany("\App\Song", "playlist_song", "song_id", "playlist_id");
    }

    public function playlists() {
        return $this->belongsToMany("\App\Playlist", "playlist_song", "song_id", "playlist_id")->distinct();
    }

    public function author() {
        return $this->belongsTo("\App\User", "user_id", "id")->first();
    }

    public function get_author_by_name($name) {
        return DB::table("users")->where("name", "=", $name)->first();
    }
}
