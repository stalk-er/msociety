<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model {

    protected $table = "genres";
    protected $fillable = [
        "name",
        "description",
    ];
    
    public function songs() {
        return $this->belongsToMany("\App\Song", "song_genre", "genre_id", "song_id");
    }
}
