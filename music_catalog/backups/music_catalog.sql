-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 
-- Версия на сървъра: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `music_catalog`
--

-- --------------------------------------------------------

--
-- Структура на таблица `activities`
--

CREATE TABLE `activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `activity_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `activities`
--

INSERT INTO `activities` (`id`, `user_id`, `activity_content`, `seen`, `created_at`, `updated_at`) VALUES
(1, 1, 'Created new account and joined the platform.', 1, '2017-04-08 13:29:02', '2017-04-18 14:50:52'),
(2, 1, '<strong>cvetan</strong> uploaded a new song called <strong>ay</strong>', 1, '2017-04-08 13:39:09', '2017-04-18 14:51:10'),
(3, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Summer Hits 2017</strong>', 1, '2017-04-10 14:54:35', '2017-04-18 14:51:13'),
(4, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Winter Hits 2016</strong>', 1, '2017-04-10 14:54:56', '2017-04-18 14:51:15'),
(5, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Spring Hits 2015</strong>', 1, '2017-04-10 14:55:03', '2017-04-18 14:51:17'),
(6, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Golden Hits 1994</strong>', 1, '2017-04-10 14:55:35', '2017-04-18 14:51:18'),
(7, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>No season, no life</strong>', 1, '2017-04-10 14:55:39', '2017-04-18 14:51:20'),
(8, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Summer Hits 2017</strong>', 1, '2017-04-10 14:55:53', '2017-04-18 14:51:22'),
(9, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Golden Hits 1994</strong>', 1, '2017-04-10 14:55:54', '2017-04-18 14:51:24'),
(10, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Winter Hits 2016</strong>', 1, '2017-04-10 14:55:56', '2017-04-18 14:51:26'),
(11, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>No season, no life</strong>', 1, '2017-04-10 14:55:59', '2017-04-18 14:51:54'),
(12, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Summer Hits 2017</strong>', 1, '2017-04-10 15:04:02', '2017-04-18 14:51:56'),
(13, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Winter Hits 2016</strong>', 1, '2017-04-10 15:06:20', '2017-04-18 14:51:59'),
(14, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Spring Hits 2015</strong>', 1, '2017-04-10 15:11:28', '2017-04-18 14:51:57'),
(15, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Golden Hits 1994</strong>', 1, '2017-04-10 15:11:38', '2017-04-18 14:52:01'),
(16, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>No season, no life</strong>', 1, '2017-04-10 16:10:15', '2017-04-18 14:52:00'),
(17, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>No season, no life</strong>', 1, '2017-04-10 16:10:23', '2017-04-18 14:52:03'),
(18, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Summer Hits 2017</strong>', 1, '2017-04-10 17:18:47', '2017-04-18 14:52:04'),
(19, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Winter Hits 2016</strong>', 1, '2017-04-10 17:18:47', '2017-04-18 14:52:06'),
(20, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Spring Hits 2015</strong>', 1, '2017-04-10 17:18:49', '2017-04-18 14:52:09'),
(21, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>Golden Hits 1994</strong>', 1, '2017-04-12 16:30:25', '2017-04-18 14:52:10'),
(22, 1, '<strong>cvetan</strong> added <strong>ay</strong> to his playlist <strong>No season, no life</strong>', 1, '2017-04-12 16:30:33', '2017-04-18 14:52:14'),
(23, 1, '<strong>cvetan</strong> uploaded a new song called <strong>Espera</strong>', 1, '2017-04-12 17:36:54', '2017-04-18 14:52:13'),
(24, 1, '<strong>cvetan</strong> uploaded a new song called <strong>An Infinte Regression</strong>', 1, '2017-04-12 17:38:35', '2017-04-18 14:52:19'),
(25, 1, '<strong>cvetan</strong> uploaded a new song called <strong>Inner Assassins</strong>', 1, '2017-04-12 17:39:22', '2017-04-18 14:52:17'),
(26, 1, '<strong>cvetan</strong> uploaded a new song called <strong>The Brain Dance</strong>', 1, '2017-04-12 17:39:50', '2017-04-18 14:52:16'),
(27, 1, '<strong>cvetan</strong> added <strong>Tosin Abasi cool song</strong> to his playlist <strong>Spring Hits 2015</strong>', 1, '2017-04-12 17:50:53', '2017-04-18 14:52:22'),
(28, 1, '<strong>cvetan</strong> added <strong>Tosin Abasi cool song</strong> to his playlist <strong>Summer Hits 2017</strong>', 1, '2017-04-12 17:50:57', '2017-04-18 14:52:20'),
(29, 1, '<strong>cvetan</strong> added <strong>Tosin Abasi cool song</strong> to his playlist <strong>No season, no life</strong>', 1, '2017-04-12 17:50:58', '2017-04-18 14:52:30'),
(30, 1, '<strong>cvetan</strong> added <strong>The Brain Dance</strong> to his playlist <strong>Summer Hits 2017</strong>', 1, '2017-04-12 17:51:06', '2017-04-18 14:52:31'),
(31, 1, '<strong>cvetan</strong> added <strong>Tosin Abasi cool song</strong> to his playlist <strong>Winter Hits 2016</strong>', 1, '2017-04-12 18:13:17', '2017-04-18 14:52:32'),
(32, 2, 'Created new account and joined the platform.', 0, '2017-04-17 15:06:46', '2017-04-17 15:06:46'),
(33, 1, '<strong>cvetan</strong> just followed <strong>Georgi</strong>', 1, '2017-04-18 14:33:33', '2017-04-18 14:52:34'),
(34, 2, '<strong>Georgi</strong> have been followed by <strong>cvetan</strong>', 0, '2017-04-18 14:33:33', '2017-04-18 14:33:33'),
(35, 1, '<strong>cvetan</strong> just unfollowed <strong>Georgi</strong>', 1, '2017-04-18 14:36:09', '2017-04-18 14:52:35'),
(36, 2, '<strong>Georgi</strong> have been unfollowed by <strong>cvetan</strong>', 0, '2017-04-18 14:36:09', '2017-04-18 14:36:09'),
(37, 1, '<strong>cvetan</strong> just followed <strong>Georgi</strong>', 1, '2017-04-18 14:44:43', '2017-04-18 14:52:39'),
(38, 2, '<strong>Georgi</strong> have been followed by <strong>cvetan</strong>', 0, '2017-04-18 14:44:43', '2017-04-18 14:44:43'),
(39, 1, '<strong>cvetan</strong> just unfollowed <strong>Georgi</strong>', 1, '2017-04-18 14:44:52', '2017-04-18 14:52:41'),
(40, 2, '<strong>Georgi</strong> have been unfollowed by <strong>cvetan</strong>', 0, '2017-04-18 14:44:52', '2017-04-18 14:44:52'),
(41, 1, '<strong>cvetan</strong> just followed <strong>Georgi</strong>', 1, '2017-04-18 14:44:58', '2017-04-18 14:52:28'),
(42, 2, '<strong>Georgi</strong> have been followed by <strong>cvetan</strong>', 0, '2017-04-18 14:44:58', '2017-04-18 14:44:58'),
(43, 1, '<strong>cvetan</strong> added <strong>Espera</strong> to his playlist <strong>Summer Hits 2017</strong>', 1, '2017-04-18 14:53:24', '2017-04-18 14:53:31'),
(44, 1, '<strong>cvetan</strong> added <strong>Espera</strong> to his playlist <strong>Spring Hits 2015</strong>', 1, '2017-04-18 14:53:24', '2017-04-18 14:53:32'),
(45, 1, '<strong>cvetan</strong> added <strong>Tosin Abasi cool song</strong> to his playlist <strong>Golden Hits 1994</strong>', 1, '2017-04-18 14:53:49', '2017-04-18 14:54:14'),
(46, 1, '<strong>cvetan</strong> added <strong>Tosin Abasi cool song</strong> to his playlist <strong>Spring Hits 2015</strong>', 1, '2017-04-18 14:53:49', '2017-04-18 14:54:12'),
(47, 1, '<strong>cvetan</strong> added <strong>Espera</strong> to his playlist <strong>Winter Hits 2016</strong>', 1, '2017-04-18 14:54:09', '2017-04-18 14:54:15'),
(48, 1, '<strong>cvetan</strong> added <strong>Inner Assassins</strong> to his playlist <strong>Summer Hits 2017</strong>', 1, '2017-04-18 14:54:27', '2017-04-18 14:54:42'),
(49, 1, '<strong>cvetan</strong> added <strong>Inner Assassins</strong> to his playlist <strong>Golden Hits 1994</strong>', 1, '2017-04-18 14:54:31', '2017-04-18 14:54:48'),
(50, 1, '<strong>cvetan</strong> added <strong>Inner Assassins</strong> to his playlist <strong>Spring Hits 2015</strong>', 1, '2017-04-18 14:54:34', '2017-04-18 14:54:46'),
(51, 1, '<strong>cvetan</strong> added <strong>Tosin Abasi cool song</strong> to his playlist <strong>Winter Hits 2016</strong>', 1, '2017-04-18 15:27:51', '2017-04-18 15:32:52'),
(52, 1, '<strong>cvetan</strong> added <strong>Espera</strong> to his playlist <strong>Golden Hits 1994</strong>', 1, '2017-04-18 15:32:45', '2017-04-18 15:32:54'),
(53, 1, '<strong>cvetan</strong> added <strong>The Brain Dance</strong> to his playlist <strong>Winter Hits 2016</strong>', 1, '2017-04-18 15:37:39', '2017-04-18 15:38:13'),
(54, 1, '<strong>cvetan</strong> added <strong>Espera</strong> to his playlist <strong>No season, no life</strong>', 1, '2017-04-18 15:37:57', '2017-04-18 15:38:12');

-- --------------------------------------------------------

--
-- Структура на таблица `followers`
--

CREATE TABLE `followers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `followed_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `followers`
--

INSERT INTO `followers` (`id`, `user_id`, `followed_by`, `created_at`, `updated_at`) VALUES
(3, 2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `genres`
--

CREATE TABLE `genres` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `genres`
--

INSERT INTO `genres` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Rock&Roll', NULL, '2017-04-07 21:00:00', '2017-04-07 21:00:00'),
(2, 'Metal Heavy', NULL, '2017-04-07 21:00:00', '2017-04-07 21:00:00'),
(3, 'Country/Folk', NULL, '2017-04-07 21:00:00', '2017-04-07 21:00:00');

-- --------------------------------------------------------

--
-- Структура на таблица `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_07_02_131051_create_user_types_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_07_02_173533_create_songs_table', 1),
('2016_07_02_173546_create_genres_table', 1),
('2016_07_02_202616_create_song_genre_table', 1),
('2016_07_05_203819_create_user_history_table', 1),
('2016_07_07_204511_create_playlists_table', 1),
('2016_07_07_205205_create_playlist_songs_table', 1),
('2016_08_07_173908_create_users_meta_table', 1),
('2016_08_07_174837_create_followers_table', 1),
('2016_08_08_183023_create_activities_table', 1);

-- --------------------------------------------------------

--
-- Структура на таблица `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура на таблица `playlists`
--

CREATE TABLE `playlists` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `playlists`
--

INSERT INTO `playlists` (`id`, `name`, `description`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Summer Hits 2017', 'The best summer hits - ever.', 1, '2017-04-10 14:38:32', '2017-04-10 14:38:32'),
(2, 'Winter Hits 2016', 'The warmest hits ever.', 1, '2017-04-10 14:42:52', '2017-04-10 14:42:52'),
(3, 'Spring Hits 2015', 'The best shiny music ever.', 1, '2017-04-10 14:46:25', '2017-04-10 14:46:25'),
(4, 'Golden Hits 1994', 'The best music there is.', 1, '2017-04-10 14:51:51', '2017-04-10 14:51:51'),
(5, 'No season, no life', 'Ебах маа му', 1, '2017-04-10 14:53:36', '2017-04-10 14:53:36');

-- --------------------------------------------------------

--
-- Структура на таблица `playlist_song`
--

CREATE TABLE `playlist_song` (
  `id` int(10) UNSIGNED NOT NULL,
  `playlist_id` int(10) UNSIGNED NOT NULL,
  `song_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `playlist_song`
--

INSERT INTO `playlist_song` (`id`, `playlist_id`, `song_id`, `created_at`, `updated_at`) VALUES
(23, 5, 1, NULL, NULL),
(24, 1, 5, NULL, NULL),
(26, 1, 2, NULL, NULL),
(27, 3, 2, NULL, NULL),
(28, 4, 1, NULL, NULL),
(29, 3, 1, NULL, NULL),
(30, 2, 2, NULL, NULL),
(31, 1, 4, NULL, NULL),
(32, 4, 4, NULL, NULL),
(33, 3, 4, NULL, NULL),
(34, 2, 1, NULL, NULL),
(35, 4, 2, NULL, NULL),
(36, 2, 5, NULL, NULL),
(37, 5, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `songs`
--

CREATE TABLE `songs` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plays` int(11) NOT NULL DEFAULT '0',
  `likes` int(11) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `duration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `songs`
--

INSERT INTO `songs` (`id`, `name`, `description`, `author`, `plays`, `likes`, `rating`, `duration`, `file`, `cover`, `user_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tosin Abasi cool song', '"\\n                                Go ahead…\\n                            "', 'cvetan', 92, 2, 0, '', '/songs/04 - Another Year/04 - Another Year.mp3', '/songs/04 - Another Year/cover.jpg', 1, NULL, '2017-04-08 13:39:09', '2017-04-18 14:53:46'),
(2, 'Espera', '"Radka Piratka"', 'cvetan', 26, 2, 0, '2:13', '/songs/09 - Espera/09 - Espera.mp3', '/songs/09 - Espera/Cover.jpg', 1, NULL, '2017-04-12 17:36:54', '2017-04-17 17:13:45'),
(3, 'An Infinte Regression', '"\\n                                01 - An Infinte Regression\\n                            "', 'cvetan', 28, 1, 0, '3:25', '/songs/01 - An Infinte Regression/01 - An Infinte Regression.mp3', '/songs/01 - An Infinte Regression/Cover.jpg', 1, NULL, '2017-04-12 17:38:35', '2017-04-17 16:34:57'),
(4, 'Inner Assassins', '"\\n                                Inner Assassins\\n                            "', 'cvetan', 20, 2, 0, '5:31', '/songs/04 Inner Assassins/04 Inner Assassins.mp3', '/songs/04 Inner Assassins/Cover.jpg', 1, NULL, '2017-04-12 17:39:22', '2017-04-18 14:53:07'),
(5, 'The Brain Dance', '"The Brain Dance\\n                            "', 'cvetan', 35, 11, 0, '7:2', '/songs/09 The Brain Dance/09 The Brain Dance.mp3', '/songs/09 The Brain Dance/Cover.jpg', 1, NULL, '2017-04-12 17:39:50', '2017-04-18 14:21:36');

-- --------------------------------------------------------

--
-- Структура на таблица `song_genre`
--

CREATE TABLE `song_genre` (
  `id` int(10) UNSIGNED NOT NULL,
  `song_id` int(10) UNSIGNED NOT NULL,
  `genre_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `song_genre`
--

INSERT INTO `song_genre` (`id`, `song_id`, `genre_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 2, 1, NULL, NULL),
(4, 2, 2, NULL, NULL),
(5, 3, 1, NULL, NULL),
(6, 3, 2, NULL, NULL),
(7, 3, 3, NULL, NULL),
(8, 4, 1, NULL, NULL),
(9, 4, 2, NULL, NULL),
(10, 5, 1, NULL, NULL),
(11, 5, 2, NULL, NULL),
(12, 5, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(10) UNSIGNED NOT NULL,
  `avatar_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `type`, `avatar_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'cvetan', 'cvetan.dimitrov@sofyma.com', '$2y$10$Nw52n9YNnOU3Kvu2NDQvIev/LU1f1GFHI3bJJ3wq4Ok4i6yNc5djO', 1, '/avatars/cvetan/cover.jpg', 'LqJI6vmze18oLpdPhfMHHuuC7GqtS0qHW24FUUiwh47YyH5ruOS74AAZsIna', '2017-04-08 13:29:02', '2017-04-18 15:42:22'),
(2, 'Georgi', 'gosho@gmail.com', '$2y$10$Nw52n9YNnOU3Kvu2NDQvIev/LU1f1GFHI3bJJ3wq4Ok4i6yNc5djO', 3, '/avatars/default/default.jpg', 'BWsHzNYvAcMCTb4YxJtQDhCD924qQyFmdXhjgUCPAimwDhKdKndB75fb4dl3', '2017-04-17 15:06:46', '2017-04-18 14:31:48');

-- --------------------------------------------------------

--
-- Структура на таблица `users_meta`
--

CREATE TABLE `users_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `fb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tw` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `followers` int(11) NOT NULL DEFAULT '0',
  `following` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `users_meta`
--

INSERT INTO `users_meta` (`id`, `user_id`, `info`, `fb`, `tw`, `gp`, `followers`, `following`, `created_at`, `updated_at`) VALUES
(1, 1, '"\\n                                                \\n                                                \\n                                                \\n                                                \\n                                            \\n                                            \\n                                            \\n                                            "', 'http://mypage.facebook.com/', 'http://twitter.com/', 'http://gp.com/', 0, 1, '2017-04-12 16:31:28', '2017-04-18 14:44:58'),
(2, 2, NULL, '', '', '', 1, 0, '2017-04-18 14:33:33', '2017-04-18 14:44:58');

-- --------------------------------------------------------

--
-- Структура на таблица `user_history`
--

CREATE TABLE `user_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `song_id` int(10) UNSIGNED NOT NULL,
  `liked` int(11) NOT NULL DEFAULT '0',
  `plays` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `user_history`
--

INSERT INTO `user_history` (`id`, `user_id`, `song_id`, `liked`, `plays`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 50, '2017-04-08 14:02:33', '2017-04-18 14:53:46'),
(2, 1, 5, 0, 1, '2017-04-12 17:42:41', '2017-04-12 17:59:58'),
(3, 1, 3, 1, 0, '2017-04-12 17:59:52', '2017-04-12 18:13:55'),
(4, 1, 4, 1, 1, '2017-04-12 18:00:08', '2017-04-18 14:53:07'),
(5, 1, 2, 1, 0, '2017-04-12 18:00:14', '2017-04-12 18:00:14'),
(18, 2, 1, 1, 26, '2017-04-17 16:09:18', '2017-04-17 18:03:53'),
(19, 2, 2, 1, 21, '2017-04-17 16:09:21', '2017-04-17 17:13:45'),
(20, 2, 5, 1, 20, '2017-04-17 16:09:23', '2017-04-18 14:21:36'),
(21, 2, 3, 0, 19, '2017-04-17 16:09:26', '2017-04-17 16:34:57'),
(23, 2, 4, 1, 3, '2017-04-17 16:32:51', '2017-04-17 17:54:26');

-- --------------------------------------------------------

--
-- Структура на таблица `user_types`
--

CREATE TABLE `user_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Musician', 'Musician', '2017-04-07 21:00:00', '2017-04-07 21:00:00'),
(3, 'Fan', 'Fan', '2017-04-07 21:00:00', '2017-04-07 21:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activities_user_id_foreign` (`user_id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `followers_user_id_foreign` (`user_id`),
  ADD KEY `followers_followed_by_foreign` (`followed_by`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `playlists`
--
ALTER TABLE `playlists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playlists_user_id_foreign` (`user_id`);

--
-- Indexes for table `playlist_song`
--
ALTER TABLE `playlist_song`
  ADD PRIMARY KEY (`id`),
  ADD KEY `playlist_song_playlist_id_foreign` (`playlist_id`),
  ADD KEY `playlist_song_song_id_foreign` (`song_id`);

--
-- Indexes for table `songs`
--
ALTER TABLE `songs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `songs_user_id_foreign` (`user_id`);

--
-- Indexes for table `song_genre`
--
ALTER TABLE `song_genre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `song_genre_song_id_foreign` (`song_id`),
  ADD KEY `song_genre_genre_id_foreign` (`genre_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_type_foreign` (`type`);

--
-- Indexes for table `users_meta`
--
ALTER TABLE `users_meta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_meta_fb_unique` (`fb`),
  ADD UNIQUE KEY `users_meta_tw_unique` (`tw`),
  ADD UNIQUE KEY `users_meta_gp_unique` (`gp`),
  ADD KEY `users_meta_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_history`
--
ALTER TABLE `user_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_history_song_id_foreign` (`song_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_types_description_unique` (`description`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `playlists`
--
ALTER TABLE `playlists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `playlist_song`
--
ALTER TABLE `playlist_song`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `songs`
--
ALTER TABLE `songs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `song_genre`
--
ALTER TABLE `song_genre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users_meta`
--
ALTER TABLE `users_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_history`
--
ALTER TABLE `user_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `followers`
--
ALTER TABLE `followers`
  ADD CONSTRAINT `followers_followed_by_foreign` FOREIGN KEY (`followed_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `followers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `playlists`
--
ALTER TABLE `playlists`
  ADD CONSTRAINT `playlists_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `playlist_song`
--
ALTER TABLE `playlist_song`
  ADD CONSTRAINT `playlist_song_playlist_id_foreign` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `playlist_song_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `songs`
--
ALTER TABLE `songs`
  ADD CONSTRAINT `songs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `song_genre`
--
ALTER TABLE `song_genre`
  ADD CONSTRAINT `song_genre_genre_id_foreign` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `song_genre_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_type_foreign` FOREIGN KEY (`type`) REFERENCES `user_types` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `users_meta`
--
ALTER TABLE `users_meta`
  ADD CONSTRAINT `users_meta_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `user_history`
--
ALTER TABLE `user_history`
  ADD CONSTRAINT `user_history_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
