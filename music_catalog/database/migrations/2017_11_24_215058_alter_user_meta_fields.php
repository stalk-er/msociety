<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserMetaFields extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('users_meta', function (Blueprint $table) {

            $table->integer('fb')->nullable()->change();

            $table->integer('tw')->nullable()->change();

            $table->integer('gp')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('users_meta');
    }

}
