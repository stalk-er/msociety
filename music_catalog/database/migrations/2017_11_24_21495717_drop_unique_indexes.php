<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUniqueIndexes extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::table('users_meta', function (Blueprint $table) {

            $table->dropUnique('users_meta_fb_unique');
            $table->dropUnique('users_meta_tw_unique');
            $table->dropUnique('users_meta_gp_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        
    }

}
