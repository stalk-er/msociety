@extends('layouts.app')
@section('content')
<section>
    <section class="hbox stretch">
        <!-- Aside -->
        @include("modals.side_menu")
        <!-- End Aside -->
        <section id="content">
            <section class="vbox to_put {{ bodyClass() }}">
                <section class="vbox activity-history" id="bjax-target">
                    <!-- Template -->
                    <table data-template="history_item" hidden>
                        <tr class="history_item">                    
                            <td></td>
                            <td class="text-success"></td>
                        </tr>
                    </table>
                    <!-- / Template -->
                    <input type="hidden" id="page_identifier" value="{{ bodyClass() }}" />
                    <section class="scrollable padder to_get activity-history">
                        <div class="m-b-md">
                            <h3 class="m-b-none">Activity History</h3>
                        </div>
                        <div class="col-sm-6">
                            <section class="panel panel-default">
                                <header class="panel-heading">History</header>
                                @if(!Auth::user())
                                <table class="table table-striped m-b-none activity_history">
                                    <thead>
                                        <tr>
                                            <th>Song</th>                    
                                            <th style="width:200px;">Last played</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                                @else
                                <table class="table table-striped m-b-none activity_history">
                                    <thead>
                                        <tr>
                                            <th>Song</th>                    
                                            <th style="width:200px;">Last played</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($history as $h)
                                        <tr class="history_item song">
                                    <input type="hidden" class="history_item_id" value="{{ $h->id }}" />
                                    <input type="hidden" id="song_id" value="{{ $h->id }}" />
                                    <td style="display: none;" class="song_meta">
                                        <input type="hidden" value="{{ $h->file }}" name="mp3" />
                                        <input type="hidden" value="{{ $h->author }}" name="artist" />
                                        <input type="hidden" value="{{ $h->name }}" name="title" />
                                        <input type="hidden" value="{{ $h->cover }}" name="poster" />
                                    </td>
                                    <td><span class="fname">{{ $h->name }}</span>
                                        <a href="#" class="jp-play-me pull-right m-t-sm m-l text-md">
                                            <i class="icon-control-play text"></i>
                                            <i class="icon-control-pause text-active"></i>
                                        </a>
                                    <td class="text-success">{{ date('F d, Y - g:i', strtotime('+3 hours', strtotime($h->updated_at))) }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @endif
                            </section>
                        </div>
                    </section>
                </section>
            </section>
            @include("modals.player")
        </section>
        @if(Auth::user())
        @include('modals.members_sidebar')
        @endif
    </section>
</section>
@endsection