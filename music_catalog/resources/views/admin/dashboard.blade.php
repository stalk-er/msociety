@extends('layouts.app')
@section('content')
<section>
    <section class="hbox stretch">
        <!-- Aside -->
        @include("modals.side_menu")
        <!-- End Aside -->
        <section id="content">
            <section class="hbox stretch ">
                <section>
                    <section class="vbox to_put {{ bodyClass() }}">
                        <section class="scrollable padder-lg w-f-md to_get" id="bjax-target">
                            <input type="hidden" id="page_identifier" value="{{ bodyClass() }}" />
                            <!-- data-toggle="class:fa-spin" -->
                            <a href="" onclick='window.location.reload()' data-bjax="" data-replace="/" data-url="/" data-target=".to_put" data-el="#bjax-target" class="pull-right text-muted m-t-lg"><i class="icon-refresh i-lg  inline"></i></a>
                            <h2 class="font-thin m-b">Discover <span class="musicbar animate inline m-l-sm" style="width:20px; height:20px;">
                                    <span class="bar1 a1 bg-primary lter"></span>
                                    <span class="bar2 a2 bg-info lt"></span>
                                    <span class="bar3 a3 bg-success"></span>
                                    <span class="bar4 a4 bg-warning dk"></span>
                                    <span class="bar5 a5 bg-danger dker"></span>
                                </span>
                            </h2>
                            <div class="row row-sm">
                                <style> .popover-content { padding: 0; } </style>
                                @foreach($songs as $song)
                                @include('modals.songBox', $song)
                                @endforeach
                            </div>
                            <!-- New songs and top songs -->
                            <div class="row">
                                <div class="col-md-5" id="top_songs_scroll">
                                    @include("modals.top_songs")
                                </div>
                            </div>

                            @if(!Auth::user())
                            <div class="row m-t-lg m-b-lg">
                                <div class="col-sm-6">
                                    <div class="bg-primary wrapper-md r">
                                        <a href="{{ url("/login") }}">
                                            <span class="h4 m-b-xs block">
                                                <i class=" icon-user-follow i-lg"></i> Login or Create account
                                            </span>
                                            <span class="text-muted">Save and share your playlist with your friends when you log in or create an account.</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif

                        </section>
                    </section>
                    @include("modals.player")
                </section>
                @if(Auth::user())
                @include('modals.members_sidebar')
                @endif
            </section>
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
    </section>
</section>
</section>
@endsection
