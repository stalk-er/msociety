@extends('layouts.app')
@section('content')
<section>
    <section class="hbox stretch">
        <!-- Aside -->
        @include("modals.side_menu")
        <!-- End Aside -->
        <section id="content">
            <section class="vbox to_put">
                <section class="scrollable to_get" id="bjax-target">
                    <section class="hbox stretch">
                        @if(Auth::user())
                        <input id="user_page_id" value="{{ $user->id }}" type="hidden" />
                        <input id="current_user_id" value="{{ Auth::user()->id }}" type="hidden" />
                        @endif

                        @include("modals.user_meta")

                        @include("modals.user_dashboard")

                        @include('modals.user_preferences')

                    </section>
                </section>
            </section>
            @include('modals.player')
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
        @if(Auth::user())
        @include('modals.members_sidebar')
        @endif
    </section>
</section>
@endsection