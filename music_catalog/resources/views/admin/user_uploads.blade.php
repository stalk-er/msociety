@extends('layouts.app')
@section('content')
<section>
    <section class="hbox stretch">

        <!-- Aside -->
        @include("modals.side_menu")
        <!-- End Aside -->

        <section id="content">
            <section class="vbox to_put {{ bodyClass() }}">
                <section class="scrollable padder to_get" id="bjax-target">
                    <input type="hidden" id="page_identifier" value="{{ bodyClass() }}" />
                    <div class="m-b-md">
                        <h3 class="m-b-none">User Uploads</h3>
                    </div>
                    <div class="col-sm-12">
                        <section class="panel panel-default uploads_history">
                            <header class="panel-heading">Songs uploaded by youself</header>
                            @include('modals.user_uploads_table')
                        </section>
                    </div>
                </section>
            </section>
            @include("modals.player")
        </section>
        @if(Auth::user())
        @include('modals.members_sidebar')
        @endif
    </section>
</section>
@endsection