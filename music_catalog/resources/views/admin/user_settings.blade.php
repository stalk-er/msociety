@extends('layouts.app')
@section('content')
<section>
    <section class="hbox stretch">
        <!-- Aside -->
        @include("modals.side_menu")
        <!-- / Aside -->
        <section id="content">
            <section class="vbox to_put">
                @foreach($errors->all(':message') as $message)
                <div id="form-messages" class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
                @endforeach()
                <section class="scrollable padder to_get" id="bjax-target">
                    <script>
                        $(document).ready(function () {

                            var submitAjaxRequest = function (e) {
                                e.preventDefault();
                                var content = $("#editor.user_meta").html();
                                $("#editor_content.user_meta").val(JSON.stringify(content));
                                var formData = new FormData(this);
                                setTimeout(function () {
                                    $.ajax({
                                        url: $("form.user_meta_form").attr("action"),
                                        type: $("form.user_meta_form").attr("method"),
                                        data: formData,
                                        headers: {
                                            'X-CSRF-TOKEN': $("meta#token").attr("content")
                                        },
                                        processData: false,
                                        contentType: false,
                                        success: function () {
                                            //action_msg("alert-success", "Settings updated!");
                                        },
                                    });
                                }, 1000);
                            };
                            $("form.user_meta_form").on('submit', submitAjaxRequest);

                        });
                    </script>


                    <div class="m-b-md">
                        <h3 class="m-b-none">Settings</h3>
                    </div>
                    <div class="col-md-12 col-sm-12 col-lg-12">
                        <section class="panel panel-default">
                            <header class="panel-heading font-bold">User settings</header>
                            <div class="panel-body">

                                <form class="user_meta_form form-horizontal" method="POST" action="/save-settings" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Username: </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="name" class="form-control" value="{{ Auth::user()->name }}">
                                            <span class="help-block m-b-none">A block of help text that breaks onto a new line and may extend beyond one line.</span>
                                        </div>
                                    </div>

                                    <div class="line line-dashed b-b line-lg pull-in"></div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">New password: </label>
                                        <div class="col-sm-8">
                                            <input type="password" name="password" value="" class="form-control">
                                            <span class="help-block m-b-none">Put your new password here.</span>
                                        </div>
                                    </div>

                                    <div class="line line-dashed b-b line-lg pull-in"></div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">File input</label>
                                        <div class="col-sm-8">
                                            <input type="file" name="avatar_image" class="filestyle" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-0" style="position: fixed; left: -5000px;">
                                            <div class="inline">
                                                <div class="thumb-lg">
                                                    <img src="{{ url("/") . "/" . Auth::user()->avatar_image }}" class="avatar_image img-circle" alt="{{ Auth::user()->avatar_image }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="line line-dashed b-b line-lg pull-in"></div>
                                    </div>

                                    <div class="line line-dashed b-b line-lg pull-in"></div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">About me: </label>
                                        <div class="col-sm-8">
                                            <div class="btn-toolbar m-b-sm btn-editor" data-role="editor-toolbar" data-target="#editor">
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a data-edit="fontName Serif" style="font-family:'Serif'">Serif</a></li><li><a data-edit="fontName Sans" style="font-family:'Sans'">Sans</a></li><li><a data-edit="fontName Arial" style="font-family:'Arial'">Arial</a></li><li><a data-edit="fontName Arial Black" style="font-family:'Arial Black'">Arial Black</a></li><li><a data-edit="fontName Courier" style="font-family:'Courier'">Courier</a></li><li><a data-edit="fontName Courier New" style="font-family:'Courier New'">Courier New</a></li><li><a data-edit="fontName Comic Sans MS" style="font-family:'Comic Sans MS'">Comic Sans MS</a></li><li><a data-edit="fontName Helvetica" style="font-family:'Helvetica'">Helvetica</a></li><li><a data-edit="fontName Impact" style="font-family:'Impact'">Impact</a></li><li><a data-edit="fontName Lucida Grande" style="font-family:'Lucida Grande'">Lucida Grande</a></li><li><a data-edit="fontName Lucida Sans" style="font-family:'Lucida Sans'">Lucida Sans</a></li><li><a data-edit="fontName Tahoma" style="font-family:'Tahoma'">Tahoma</a></li><li><a data-edit="fontName Times" style="font-family:'Times'">Times</a></li><li><a data-edit="fontName Times New Roman" style="font-family:'Times New Roman'">Times New Roman</a></li><li><a data-edit="fontName Verdana" style="font-family:'Verdana'">Verdana</a></li></ul>
                                                </div>
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a data-edit="fontSize 5" style="font-size:24px">Huge</a></li>
                                                        <li><a data-edit="fontSize 3" style="font-size:18px">Normal</a></li>
                                                        <li><a data-edit="fontSize 1" style="font-size:14px">Small</a></li>
                                                    </ul>
                                                </div>
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm" data-edit="bold" title="" data-original-title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="italic" title="" data-original-title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="strikethrough" title="" data-original-title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="underline" title="" data-original-title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
                                                </div>
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm" data-edit="insertunorderedlist" title="" data-original-title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="insertorderedlist" title="" data-original-title="Number list"><i class="fa fa-list-ol"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="outdent" title="" data-original-title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="indent" title="" data-original-title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                                </div>
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm btn-info" data-edit="justifyleft" title="" data-original-title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="justifycenter" title="" data-original-title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="justifyright" title="" data-original-title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="justifyfull" title="" data-original-title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
                                                </div>
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="" data-original-title="Hyperlink"><i class="fa fa-link"></i></a>
                                                    <div class="dropdown-menu">
                                                        <div class="input-group m-l-xs m-r-xs">
                                                            <input class="form-control input-sm" placeholder="URL" type="text" data-edit="createLink">
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-default btn-sm" type="button">Add</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a class="btn btn-default btn-sm" data-edit="unlink" title="" data-original-title="Remove Hyperlink"><i class="fa fa-cut"></i></a>
                                                </div>

                                                <div class="btn-group hide">
                                                    <a class="btn btn-default btn-sm" title="" id="pictureBtn" data-original-title="Insert picture (or just drag &amp; drop)"><i class="fa fa-picture-o"></i></a>
                                                    <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" style="opacity: 0; position: absolute; top: 0px; left: 0px; width: 0px; height: 0px;">
                                                </div>
                                                <div class="btn-group">
                                                    <a class="btn btn-default btn-sm" data-edit="undo" title="" data-original-title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
                                                    <a class="btn btn-default btn-sm" data-edit="redo" title="" data-original-title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
                                                </div>
                                            </div>
                                            <div id="editor" class="user_meta form-control" style="overflow:scroll;height:150px;max-height:150px" contenteditable="true">
                                                {!! (isset(Auth::user()->meta)) ? json_decode(Auth::user()->meta->info) : "" !!}
                                            </div>
                                        </div>
                                        <input type="hidden" id="editor_content" class="user_meta" name="info" value='{{ (isset(Auth::user()->meta)) ? json_decode(Auth::user()->meta->info) : "" }}' />
                                    </div>
                                    <div class="line line-dashed b-b line-lg pull-in"></div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Facebook: </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="fb" class="form-control" value="{{ (!empty(Auth::user()->meta->fb)) ? Auth::user()->meta->fb : "" }}">
                                            <span class="help-block m-b-none">Put a link to your facebook profile here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Twitter: </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="tw" class="form-control" value="{{ (!empty(Auth::user()->meta->tw)) ? Auth::user()->meta->tw : "" }}">
                                            <span class="help-block m-b-none">Put a link to your twitter profile here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Google Plus: </label>
                                        <div class="col-sm-8">
                                            <input type="text" name="gp" class="form-control" value="{{ (!empty(Auth::user()->meta->gp)) ? Auth::user()->meta->gp : "" }}">
                                            <span class="help-block m-b-none">Put a link to your google accout here.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                            <a href="/" class="btn btn-default">Cancel</a>
                                            <button id="submit" type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </form>
                        </section>
                    </div>

                    <!-- file input -->
                    <script src="{{ asset('js/file-input/bootstrap-filestyle.min.js') }}"></script>
                </section>
            </section>
            @include("modals.player")
        </section>
        @if(Auth::user())
        @include('modals.members_sidebar')
        @endif
    </section>
</section>
@stop