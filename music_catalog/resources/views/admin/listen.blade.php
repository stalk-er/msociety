@extends('layouts.app')
@section('content')
<section>
    <section class="hbox stretch">
        <!-- Aside -->
        @include("modals.side_menu")
        <!-- End Aside -->
        <section id="content">
            <section class="vbox to_put {{ bodyClass() }}">
                <section class="w-f-md" id="bjax-target">
                    <input type="hidden" id="page_identifier" value="{{ bodyClass() }}" />
                    <section class="hbox stretch bg-black dker">

                        <!-- Featured songs -->
                        @include('modals.featuredSongs')
                        <!-- /Featured songs -->

                        <!-- Most popular songs -->
                        @include('modals.most_popular_side')
                        <!-- /Most popular songs -->

                        <section class="col-sm-3 no-padder lt">
                            <section class="vbox">
                                <section class="scrollable hover">
                                    <div class="m-t-n-xxs">
                                        <div class="item pos-rlt find-banner">
                                            <a href="#" class="item-overlay active opacity wrapper-md font-xs">
                                                <span class="block h3 font-bold text-info">Find</span>
                                                <span class="text-muted">Search the music you like</span>
                                                <span class="bottom wrapper-md block">- <i class="icon-arrow-right i-lg pull-right"></i></span>
                                            </a>
                                            <a href="#">
                                                <img class="img-full" src="{{ url('/images/m40.jpg') }}" alt="...">
                                            </a>
                                        </div>

                                        <div class="item pos-rlt listen_banner">
                                            <a href="{{ url("/") }}/listen" data-bjax="" data-replace="{{ url("/") }}/listen" data-url="{{ url("/") }}/listen" data-target=".to_put" data-el="#bjax-target" class="item-overlay active opacity wrapper-md font-xs text-right">
                                                <span class="block h3 font-bold text-warning text-u-c">Listen</span>
                                                <span class="text-muted">Find the peace in your heart</span>
                                                <span class="bottom wrapper-md block"><i class="icon-arrow-right i-lg pull-left"></i> -</span>
                                            </a>
                                            <a href="#">
                                                <img class="img-full" src="{{ url('/images/m41.jpg') }}" alt="...">
                                            </a>
                                        </div>

                                        <div class="item pos-rlt">
                                            <a href="{{ url("/") }}/genres" data-bjax="" data-replace="{{ url("/") }}/genres" data-url="{{ url("/") }}/genres" data-target=".to_put" data-el="#bjax-target" class="item-overlay active opacity wrapper-md font-xs text-right">
                                                <span class="block h3 font-bold text-white text-u-c">{{ date("Y") }}</span>
                                                <span class="text-muted">Find, Listen &amp; Share</span>
                                                <span class="bottom wrapper-md block"><i class="icon-arrow-right i-lg pull-left"></i> -</span>
                                            </a>
                                            <a href="{{ url("/genres") }}" data-bjax="" data-replace="{{ url("/genres") }}" data-url="{{ url("/genres") }}" data-target=".to_put" data-el="#bjax-target">
                                                <img class="img-full" src="{{ url('/images/m44.jpg') }}" alt="...">
                                            </a>
                                        </div>

                                        @include('modals.new_users')
                                    </div>
                                </section>
                            </section>
                        </section>
                    </section>
                </section>
            </section>
            @include("modals.player")
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
        @if(Auth::user())
        @include('modals.members_sidebar')
        @endif
    </section>
</section>
@endsection