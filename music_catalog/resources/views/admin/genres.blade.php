@extends('layouts.app')
@section('content')
<section>
    <section class="hbox stretch">

        <!-- Aside -->
        @include("modals.side_menu")
        <!-- End Aside -->

        <section id="content">

            <section class="vbox to_put {{ bodyClass() }}">
                <section class="w-f-md" id="bjax-target">
                    <input type="hidden" id="page_identifier" value="{{ bodyClass() }}" />
                    <section class="hbox stretch">

                        <!-- genres side content -->
                        @include("modals.genres_sidebar")
                        <!-- / genres side content -->

                        <!-- songs by genre -->
                        <section id="songs_by_genre">
                            <section class="vbox">
                                <section class="scrollable padder-lg">
                                    <h2 class="font-thin m-b genre_main_title">All</h2>
                                    <div class="row row-sm">

                                        @foreach($songs as $song)
                                        @include('modals.songBox', $song)
                                        @endforeach
                                    </div>

                                    {{ $songs->render() }}
                                </section>                    
                            </section>
                        </section>
                        <!-- / songs by genre -->
                    </section>
                </section>
            </section>
            @include("modals.player")
            <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
        </section>
        @if(Auth::user())
        @include('modals.members_sidebar')
        @endif
    </section>
</section>    
</section>
@endsection