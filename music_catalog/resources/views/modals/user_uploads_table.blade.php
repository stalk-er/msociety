<style>
    td .fa-edit, td .delete_song {
        display: none;
        position: absolute;
        top: 50%;
        font-size: 20px;
        transform: translate(0, -50%);
    }
    td .delete_song {
        right: 40px;
    }
    td .fa-edit {
        right: 10px;
        line-height: 40px;
    }
    td .fa-edit.active {
        display: none;
    }
    td .delete_song, td.active .delete_song {
        display: none;
    }

    td:hover .fa-edit { 
        display: block;
    }
    td:hover .delete_song { 
        display: block;
    }

    td.active:hover .fa-edit { 
        display: none;
    }
    td.active:hover .delete_song { 
        display: none;
    }



    td .fa.fa-save {
        font-size: 20px;
        margin-left: 5px;
    }

    td .fa.fa-times {
        font-size: 22px;
        margin-left: 5px;
    }

</style>
<table class="table table-striped m-b-none">
    <script>
        function close_opened_edits() {
            $(".uploads_history").find("td.active").each(function () {
                $(this).find("input[name='new_name']").hide();
                $(this).find("i.fa-save").hide();
                $(this).find("i.fa-times").hide();
                $(this).find("i.fa-edit").css("display", "");
                $(this).find("span.song_name").show();
                $(this).removeClass("active");
            });
        }

        function delete_song(item) {
            item = $(item);

            if (confirm("Are you sure you want to delete this song ?")) {
                var did = parseInt(item.data("delete"));
                $.ajax({
                    url: window.location.origin + "/delete/song",
                    type: "post",
                    data: {
                        _token: $("meta#token").attr("content"),
                        song_id: did
                    },
                    success: function () {
                        item.closest("tr").fadeOut(function () {
                            $(this).remove();
                        });
                    }
                });
            }
            else
                return false;
        }



        function edit_song_name(item) {
            item = $(item);
            close_opened_edits();

            var edit_btn = item;
            edit_btn.closest("td").addClass("active");
            edit_btn.hide();

            var id = parseInt(item.data("edit"));
            var parent = item.closest("td");

            parent.find("span.song_name").hide();
            parent.find("input[name='new_name']").show().focus();
            parent.find("i.fa-save").show();
            parent.find("i.fa-times:not(i.fa-times.delete_song)").show();
            parent.find("i.fa-save").click(function () {
                parent.find("input[name='new_name']").focusout();
                parent.find("input[name='new_name']").hide();
                parent.find("i.fa-save").hide();
                parent.find("i.fa-times:not(i.fa-times.delete_song)").hide();

                var name = parent.find("input[name='new_name']").val();
                var url = window.location.origin + "/song/name/edit";
                var token = $("head").find("meta[name='token']").attr("content");
                var data = {
                    _token: token,
                    song_id: id,
                    song_name: name
                };

                if (name !== parent.find("span.song_name").text()) {
                    parent.find("i.fa-save").unbind("click");
                    $.ajax({
                        url: url,
                        type: "post",
                        data: data,
                        success: function () {
                            parent.find("span.song_name").text(name);
                            action_msg("alert-success", "The song name has been successfully updated !");
                        }
                    });
                    $(this).unbind("click");
                }
                else {
                    action_msg("alert-warning", "The song name is not changed!");
                    $(this).unbind("click");
                }
                parent.find("span.song_name").show();
                edit_btn.parent().removeClass("active");
                edit_btn.show();
            });
            parent.find("i.fa-times:not(i.fa-times.delete_song)").click(function (e) {
                e.preventDefault();
                parent.find("span.song_name").show();
                parent.find("i.fa-save").hide();
                $(this).hide();
                parent.find("input[name='new_name']").hide();

                edit_btn.parent().removeClass("active");
                edit_btn.show();
            });
        }
    </script>
    <thead>
        <tr>
            <th>Name</th>                    
            <th>Description</th>
            <th>Plays</th>
            <th>Likes</th>
            <th style="width:120px;">Duration</th>
            <th style="width:200px;">Last updated</th>
        </tr>
    </thead>
    <tbody>
        @if(count($user_uploads) > 0)
        @foreach($user_uploads as $upload)
        <tr>
            <td style="position: relative;">
                <span class="song_name">{{ $upload->name }}</span>
                <input type="text" style="display: none;" value="{{ $upload->name }}" name="new_name" />

                <i class="fa fa-save" style="display: none;"></i>
                <i class="fa fa-times" style="display: none;"></i>

                <i data-edit="{{ $upload->id }}" class="fa fa-edit edit_name" onclick="edit_song_name(this)"></i>
                <i data-delete="{{ $upload->id }}" class="fa fa-times delete_song" onclick="delete_song(this)"></i>
            </td>
            <td>{!! json_decode($upload->description) !!}</td>
            <td>{{ $upload->plays }}</td>
            <td>{{ $upload->likes }}</td>
            <td>{{ $upload->duration }}</td>
            <td class="text-success">{{ date('F d, Y - g:i', strtotime('+3 hours', strtotime($upload->updated_at))) }}</td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="6">No uploads found.</td>
        </tr>
        @endif
    </tbody>
</table>