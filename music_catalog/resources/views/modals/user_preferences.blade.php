<aside class="col-lg-3 b-l">
    <section class="vbox">
        <section class="scrollable padder-v">
            <div class="panel">
                <h4 class="font-thin padder">Last listened</h4>
                <div class="panel-body">
                    @if(count($last_listened) <= 0)
                    <p>No songs played yet.</p>
                    @else
                    @foreach($last_listened as $song)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 song">
                        <input type="hidden" id="song_id" value="{{ $song->id }}" />
                        <span hidden class="song_meta">
                            <input type="hidden" value="{{ $song->file }}" name="mp3" />
                            <input type="hidden" value="{{ $song->author }}" name="artist" />
                            <input type="hidden" value="{{ $song->name }}" name="title" />
                            <input type="hidden" value="{{ $song->cover }}" name="poster" />
                        </span>
                        <div class="item">
                            <div class="pos-rlt">
                                <div class="top">
                                    <span class="pull-right m-t-sm m-r-sm badge bg-info plays">{{ $song->plays }}</span>
                                </div>
                                <div class="item-overlay opacity r r-2x bg-black">
                                    <div class="center text-center m-t-n">
                                        <a href="" data-toggle="class">
                                            <i class="icon-control-play i-2x text"></i>
                                            <i class="icon-control-pause i-2x text-active"></i>
                                        </a>
                                    </div>

                                    @if(Auth::user())
                                    <div class="bottom padder m-b-sm">
                                        <a href="#" class="pull-right {{ (Auth::user()->likes($song)) ? 'active' : "" }} "  data-toggle="class">
                                            <i class="fa fa-heart-o text"></i>
                                            <i class="fa fa-heart text-active text-danger"></i>
                                            <input type="hidden" value="{{ $song->id }}" name="song_id" />
                                        </a>
                                        <a href="#">
                                            <i class="fa fa-plus-circle add_to_playlist"
                                               data-toggle="popover"
                                               data-container="#content"
                                               data-html="true"
                                               data-placement="bottom"
                                               tabindex="0"
                                               data-content='<div class="dropdown dropup pull-left"><ul class="dropdown-menu pos-stc inline" role="menu">@if(count(Auth::user()->playlists()->get()) <= 0)<li><a>No playlists found.</a></li>@else @foreach(Auth::user()->playlists()->get() as $playlist)<li><input type="hidden" id="song_id" value="{{ $song->id }}" />@if( $playlist->contains($song) )<input type="hidden" name="in_playlist" />@endif<a tabindex="-1" href="#" @if( $playlist->contains($song) ) id="removeFromPlaylist" @else id="addToPlaylist" @endif data-playlist-id="{{ $playlist->id }}">{{ $playlist->name }}
                                               @if( $playlist->contains($song) )<i class="fa fa-check pull-right"></i>@endif</a></li>@endforeach @endif</ul></div>'
                                               title=""
                                               data-animation="true"
                                               data-original-title='Add to playlist'
                                               ></i>
                                        </a>
                                    </div>
                                    @endif
                                </div>

                                <a href="#"><img src="{{ $song->cover }}" alt="" class="r r-2x img-full" /></a>
                            </div>

                            <div class="padder-v">
                                <a href="#" class="text-ellipsis">{{ $song->name }}</a>
                                <a href="{{ url('/') }}/user/{{ $song->user_id }}" data-bjax="" data-replace="{{ url('/') }}/user/{{ $song->user_id }}" data-url="{{ url('/') }}/user/{{ $song->user_id }}" data-target=".to_put" data-el="#bjax-target" class="text-ellipsis text-xs text-muted">{{ $song->author }}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>

            <!--
            <div class="panel clearfix">
                <h4 class="font-thin padder">Last listened</h4>
                <ul class="list-group">
                    <li class="list-group-item">
                        <p>Wellcome <a href="#" class="text-info">@Drew Wllon</a> and play this web application template, have fun1 </p>
                        <small class="block text-muted"><i class="fa fa-clock-o"></i> 2 minuts ago</small>
                    </li>
                </ul>
            </div>
            -->

        </section>
    </section>
</aside>
