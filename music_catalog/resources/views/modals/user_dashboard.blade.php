<aside class="bg-white" id="user_dashboard">
    <section class="vbox">
        <header class="header bg-light lt">
            <ul class="nav nav-tabs nav-white">
                <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li>
                <!--
                <li class=""><a href="#events" data-toggle="tab">Events</a></li>
                -->
                @if($user->hierarchy_type()->name !== "Fan" )
                <li class=""><a href="#uploads" data-toggle="tab">User Uploads</a></li>
                @endif
            </ul>
        </header>
        <section class="scrollable">
            <div class="tab-content">

                <!-- Activities -->
                <div class="tab-pane active" id="activity">
                    <ul class="list-group no-radius m-b-none m-t-n-xxs list-group-lg no-border">
                        @foreach($activities as $activity)
                        <li class="list-group-item">
                            <a href="#" class="clear">
                                <small class="pull-right">{{ $activity->time_since( strtotime( $activity->created_at->format('Y-m-d H:i:s') ) ) }} ago</small>
                                <small>{!! $activity->activity_content !!}</small>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- / Activities -->

                <!-- Events -->
                <!--
                <div class="tab-pane" id="events">
                                    <div class="text-center wrapper">
                                        <i class="fa fa-spinner fa fa-spin fa fa-large"></i>
                                    </div>
                                </div>
                -->
                <!-- / Events -->


                @if($user->hierarchy_type()->name !== "Fan" )
                <!-- Uploads -->
                <div class="tab-pane" id="uploads">
                    <div class="text-center wrapper">
                        <div class='row'>
                            @if(count($user_uploads) <= 0)
                            <p>User still has no uploads.</p>
                            @else
                            @foreach($user_uploads as $song)
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 song">
                                <input type="hidden" id="song_id" value="{{ $song->id }}" />
                                <span hidden class="song_meta">
                                    <input type="hidden" value="{{ $song->file }}" name="mp3" />
                                    <input type="hidden" value="{{ $song->author }}" name="artist" />
                                    <input type="hidden" value="{{ $song->name }}" name="title" />
                                    <input type="hidden" value="{{ $song->cover }}" name="poster" />
                                </span>
                                <div class="item">
                                    <div class="pos-rlt">
                                        <div class="top">
                                            <span class="pull-right m-t-sm m-r-sm badge bg-info plays">{{ $song->plays }}</span>
                                        </div>
                                        <div class="item-overlay opacity r r-2x bg-black">
                                            <div class="center text-center m-t-n">
                                                <a href="" data-toggle="class">
                                                    <i class="icon-control-play i-2x text"></i>
                                                    <i class="icon-control-pause i-2x text-active"></i>
                                                </a>
                                            </div>

                                            @if(Auth::user())
                                            <div class="bottom padder m-b-sm">
                                                <a href="#" class="pull-right {{ (Auth::user()->likes($song)) ? 'active' : "" }} "  data-toggle="class">
                                                    <i style="font-size: 22px;" class="fa fa-heart-o text"></i>
                                                    <i style="font-size: 22px;" class="fa fa-heart text-active text-danger"></i>
                                                    <input type="hidden" value="{{ $song->id }}" name="song_id" />
                                                </a>
                                                <a href="#">
                                                    <i style="font-size: 22px;" class="fa fa-plus-circle add_to_playlist"
                                                       data-toggle="popover"
                                                       data-container="#content"
                                                       data-html="true"
                                                       data-placement="bottom"
                                                       data-content='
                                                       <div class="dropdown dropup pull-left">
                                                       <ul class="dropdown-menu pos-stc inline" role="menu">
                                                       @if(count(Auth::user()->playlists()->get()) <= 0)
                                                       <li><a>No playlists found.</a></li>
                                                       @else
                                                       @foreach(Auth::user()->playlists()->get() as $playlist)
                                                       <li>
                                                       <input type="hidden" id="song_id" value="{{ $song->id }}" />
                                                       @if( $playlist->contains($song->id) )
                                                       <input type="hidden" name="in_playlist" />
                                                       @endif
                                                       <a tabindex="-1" href="#" @if( $playlist->contains($song->id) ) id="removeFromPlaylist" @else id="addToPlaylist" @endif data-playlist-id="{{ $playlist->id }}">
                                                       {{ $playlist->name }}
                                                       @if( $playlist->contains($song->id) )
                                                       <i class="fa fa-check pull-right"></i>
                                                       @endif
                                                       </a>
                                                       </li>
                                                       @endforeach
                                                       @endif
                                                       </ul>
                                                       </div>'
                                                       data-animation="true"
                                                       data-original-title='<button type="button" class="close pull-right" data-dismiss="popover"> &times; </button>Add to playlist'
                                                       ></i>
                                                </a>
                                            </div>
                                            @endif
                                        </div>

                                        <a href="#"><img src="{{ $song->cover }}" alt="" class="r r-2x img-full" /></a>
                                    </div>

                                    <div class="padder-v">
                                        <a href="" class="text-ellipsis">{{ $song->name }}</a>
                                        <a href="{{ url('/') }}/user/{{ $song->user_id }}" data-bjax="" data-replace="{{ url('/') }}/user/{{ $song->user_id }}" data-url="{{ url('/') }}/user/{{ $song->user_id }}" data-target=".to_put" data-el="#bjax-target" class="text-ellipsis text-xs text-muted">{{ $song->author }}</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                <!-- / Uploads -->
                @endif
            </div>
        </section>
    </section>
</aside>
