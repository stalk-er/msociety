<aside class="aside bg-light dk genres_list" id="sidebar">
    <section class="vbox animated fadeInUp">
        <section class="scrollable hover">
            <div class="list-group no-radius no-border no-bg m-t-n-xxs m-b-none auto">
                <a href="/genres/" data-bjax="" data-url="/genres" data-replace="/genres" data-target="#songs_by_genre" data-el="#songs_by_genre .vbox" class="list-group-item {{ (isset($single_genre)) ? '' : 'active' }}">All</a>
                @foreach($genres as $genre)
                <a href="/genres/{{ $genre->id }}" data-bjax="" data-url="/genres/{{ $genre->id }}" data-replace="/genres/{{ $genre->id }}" data-target="#songs_by_genre" data-el="#songs_by_genre .vbox" class="list-group-item {{ (isset($single_genre) && $single_genre->id == $genre->id) ? 'active' : '' }}" data-genre="{{ $genre->id }}">{{ $genre->name }}</a>
                @endforeach
            </div>
        </section>
    </section>
</aside>
