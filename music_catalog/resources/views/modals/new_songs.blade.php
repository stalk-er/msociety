<h3 class="font-thin">New Songs</h3>
<div class="row row-sm">
    @foreach($new_songs as $new_song)
    <div class="col-xs-6 col-sm-3">
        <div class="item song">
            <input type="hidden" id="song_id" value="{{ $new_song->id }}" />
            <span hidden class="song_meta">
                <input type="hidden" value="{{ $new_song->file }}" name="mp3" />
                <input type="hidden" value="{{ $new_song->author }}" name="artist" />
                <input type="hidden" value="{{ $new_song->name }}" name="title" />
                <input type="hidden" value="{{ $new_song->cover }}" name="poster" />
            </span>
            <div class="pos-rlt">
                <div class="item-overlay opacity r r-2x bg-black">
                    <div class="center text-center m-t-n">
                        <a href="#"><i class="fa fa-play-circle i-2x"></i></a>
                    </div>
                </div>
                <a href="#"><img src="{{ $new_song->cover }}" alt="" class="r r-2x img-full"></a>
            </div>
            <div class="padder-v">
                <a href="" class="text-ellipsis">{{ $new_song->name }}</a>
                <a href="{{ url('/') }}/user/{{ $new_song->user_id }}" data-bjax="" data-replace="{{ url('/') }}/user/{{ $new_song->user_id }}" data-url="{{ url('/') }}/user/{{ $new_song->user_id }}" data-target=".to_put" data-el="#bjax-target" class="text-ellipsis text-xs text-muted">{{ $new_song->author }}</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
