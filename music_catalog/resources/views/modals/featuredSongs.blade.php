<aside class="col-sm-5 no-padder" id="song_preview">
    <section class="vbox animated fadeInUp">
        <section class="scrollable">

            <!-- List item template to clone -->
            @include('modals.song_preview')
            @if(Auth::user())
            <h2 class="font-thin m-b genre_main_title" style="margin-left: 15px;">Featured</h2>
            @else
            <h2 class="font-thin m-b genre_main_title" style="margin-left: 15px;">Most liked</h2>
            @endif
            <ul class="list-group list-group-lg no-bg auto m-b-none m-t-n-xxs" id="featured">
                @foreach($featuredSongs as $featSong)

                <li class="list-group-item song featured" @if(Auth::user()) data-played="false" @endif>
                    <input type="hidden" id="song_id" value="{{ $featSong->id }}" />
                    <span hidden class="song_meta">
                        <input type="hidden" value="{{ $featSong->file }}" name="mp3" />
                        <input type="hidden" value="{{ $featSong->author }}" name="artist" />
                        <input type="hidden" value="{{ $featSong->name }}" name="title" />
                        <input type="hidden" value="{{ $featSong->cover }}" name="poster" />
                        <input type="hidden" value="{{ $featSong->url }}" name="url" />
                    </span>

                    @if(Auth::user())
                    <a href="#" class="m-r-sm pull-right {{ (Auth::user()->likes($featSong)) ? 'active' : "" }} "  data-toggle="class">
                        <i class="fa fa-heart-o text"></i>
                        <i class="fa fa-heart text-active text-danger"></i>
                        <input type="hidden" value="{{ $featSong->id }}" name="song_id" />
                    </a>
                    <a href="#" class="m-r-sm pull-right">
                        <i class="fa fa-plus-circle add_to_playlist"
                           data-toggle="popover"
                           data-container="#content"
                           data-html="true"
                           data-placement="bottom"
                           tabindex="0"
                           data-content='<div class="dropdown dropup pull-left"><ul class="dropdown-menu pos-stc inline" role="menu">@if(count(Auth::user()->playlists()->get()) <= 0)<li><a>No playlists found.</a></li>@else @foreach(Auth::user()->playlists()->get() as $playlist)<li><input type="hidden" id="song_id" value="{{ $featSong->id }}" />@if( $playlist->contains($featSong) )<input type="hidden" name="in_playlist" />@endif<a tabindex="-1" href="#" @if( $playlist->contains($featSong) ) id="removeFromPlaylist" @else id="addToPlaylist" @endif data-playlist-id="{{ $playlist->id }}">{{ $playlist->name }}
                           @if( $playlist->contains($featSong) )<i class="fa fa-check pull-right"></i>@endif</a></li>@endforeach @endif</ul></div>'
                           title=""
                           data-animation="true"
                           data-original-title='Add to playlist'
                           ></i>
                    </a>
                    @endif

                    <a href="#" class="jp-play-me m-r-sm pull-left">
                        <i class="icon-control-play text"></i>
                        <i class="icon-control-pause text-active"></i>
                    </a>
                    <div class="clear text-ellipsis">
                        <span class="related_song_name">{{ $featSong->name }}</span>
                        <span class="text-muted" class="song_duration">{{ $featSong->duration }}</span>
                    </div>
                </li>
                @endforeach
            </ul>
            <!-- /List item template to clone -->

        </section>
    </section>
</aside>
