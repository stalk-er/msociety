<!-- List item template to clone -->
<div class="song song_preview_container m-t-n-xxs item pos-rlt" style="height: 270px; overflow: hidden;" >
    <div class="bottom gd bg-info wrapper-lg">
        <span class="h2 font-thin song_name"></span>
        <div class="text-sm font-thin song_desc"></div>
        <a href="" class="pull-right text-sm text-muted song_author"></a>
    </div>
    
    @if(isset($singleSong))
    <input type="hidden" id="song_id" value="{{ $singleSong->id }}" />
    <span hidden class="song_meta">
        <input type="hidden" value="{{ $singleSong->file }}" name="mp3" />
        <input type="hidden" value="{{ $singleSong->author }}" name="artist" />
        <input type="hidden" value="{{ $singleSong->name }}" name="title" />
        <input type="hidden" value="{{ $singleSong->cover }}" name="poster" />
        <input type="hidden" value="{{ $singleSong->url }}" name="url" />
    </span>
    <img class="img-full song_preview_cover" src="{{ $singleSong->cover }}" alt="">
    @else
    <img class="img-full song_preview_cover" src="{{ url('/images/m40.jpg') }}" alt="">
    @endif
</div>
