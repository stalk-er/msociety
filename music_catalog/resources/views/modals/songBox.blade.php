<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 song" data-url="{{ $song->url }}">

    <input type="hidden" id="song_id" value="{{ $song->id }}" />
    <div hidden class="song_meta">
        <input type="hidden" value="{{ $song->file }}" name="mp3" />
        <input type="hidden" value="{{ $song->author }}" name="artist" />
        <input type="hidden" value="{{ $song->name }}" name="title" />
        <input type="hidden" value="{{ $song->cover }}" name="poster" />
        <input type="hidden" value="{{ $song->url }}" name="url" />
    </div>

    <div class="item">
        <div class="pos-rlt">
            <div class="top">
                <span class="pull-right m-t-sm m-r-sm badge bg-info plays">{{ $song->plays }}</span>
            </div>
            <div class="item-overlay opacity r r-2x bg-black">
                <div class="center text-center m-t-n">
                    <a href="#" data-toggle="class">
                        <i class="icon-control-play i-2x text"></i>
                        <i class="icon-control-pause i-2x text-active"></i>
                    </a>
                </div>

                @if(Auth::user())
                <div class="bottom padder m-b-sm">
                    <a href="#" class="pull-right {{ (Auth::user()->likes($song)) ? 'active' : "" }}" data-toggle="class">
                        <i class="fa fa-heart-o text"></i>
                        <i class="fa fa-heart text-active text-danger"></i>
                        <input type="hidden" value="{{ $song->id }}" name="song_id" />
                    </a>
                    <a href="#">
                        <i class="fa fa-plus-circle add_to_playlist"
                           data-toggle="popover"
                           data-container="#content"
                           data-html="true"
                           data-placement="bottom"
                           tabindex="0"
                           data-content='<div class="dropdown dropup pull-left"><ul class="dropdown-menu pos-stc inline" role="menu">@if(count(Auth::user()->playlists()->get()) <= 0)<li><a>No playlists found.</a></li>@else @foreach(Auth::user()->playlists()->get() as $playlist)<li><input type="hidden" id="song_id" value="{{ $song->id }}" />@if( $playlist->contains($song) )<input type="hidden" name="in_playlist" />@endif<a tabindex="-1" href="#" @if( $playlist->contains($song) ) id="removeFromPlaylist" @else id="addToPlaylist" @endif data-playlist-id="{{ $playlist->id }}">{{ $playlist->name }}
                           @if( $playlist->contains($song) )<i class="fa fa-check pull-right"></i>@endif</a></li>@endforeach @endif</ul></div>'
                           title=""
                           data-animation="true"
                           data-original-title='Add to playlist'
                           ></i>
                    </a>

                    <a data-animation="true" data-original-title="Share" data-content="
                       <div style='padding: 10px;'>
                       <h4 class='text-left'>Share it</h4>
                       <div class='row'>
                       <div class='col-lg-8 col-sm-12 col-md-8'><input type='text' class='form-control' value='{{ url('/listen/song/') . '/' . $song->url }}' /></div>
                       <div class='col-lg-4 col-sm-12 col-md-4'><button class='song-to-clipboard btn btn-info btn-md' data-clipboard-text='{{ url('/listen/song/') . '/' . $song->url }}'>Copy</button></div>
                       </div>

                       </div>" data-html="true" data-placement="bottom" data-container="#content" data-toggle="popover" href="{{ url("/listen/song/") . "/" .$song->url }}" class="m-r-sm pull-right"><i class="fa fa-share"></i></a>
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ url("/") . "/listen/song/" . $song->url }}" class="song-share-fb m-r-sm pull-right"><i class="fa fa-facebook"></i></a>
                    <a href="{{ url("/") . "/songs/".$song->id . "/download"  }}" class=" m-r-sm pull-right"><i class="fa fa-download"></i></a>
                </div>
                @endif
            </div>
            <a href="#"><img src="{{ $song->cover }}" alt="" class="r r-2x img-full"></a>
        </div>

        <div class="padder-v">
            <a href="#" class="text-ellipsis">{{ $song->name }}</a>
            <a href="{{ url('/') }}/user/{{ $song->user_id }}" data-bjax="" data-replace="{{ url('/') }}/user/{{ $song->user_id }}" data-url="{{ url('/') }}/user/{{ $song->user_id }}" data-target=".to_put" data-el="#bjax-target" class="text-ellipsis text-xs text-muted">{{ $song->author }}</a>
        </div>
    </div>
</div>