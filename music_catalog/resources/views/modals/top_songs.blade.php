
@if($top_songs)
<h3 class="font-thin">Top Songs</h3>
<div class="list-group bg-white list-group-lg no-bg auto">                          
    @foreach($top_songs as $top_song)
    <li class="list-group-item clearfix song">
        <span class="pull-right h2 text-muted m-l text-center plays">{{ $top_song->plays }} <div class="text-ellipsis text-xs text-muted">plays</div></span>
        <span class="pull-left thumb-sm avatar m-r">
            <img src="{{ $top_song->cover }}" alt="{{ $top_song->name }}">
        </span>
        <span class="clear">
            <span>{{ $top_song->name }}</span>
            <a 
                href="{{ 'user/'.$top_song->user_id }}"
                data-bjax="{{ url('user/'.$top_song->user_id) }}" 
                data-url="{{ url('user/'.$top_song->user_id) }}" 
                data-replace="{{ 'user/'.$top_song->user_id }}" 
                data-target=".to_put" 
                data-el="#bjax-target">
                <small class="text-muted clear text-ellipsis">by {{ $top_song->author }}</small>
            </a>
        </span>
    </li>
    @endforeach
</div>
@endif