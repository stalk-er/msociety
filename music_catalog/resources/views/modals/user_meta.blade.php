<aside class="aside-lg bg-light lter b-r">
    <section class="vbox">
        <section class="scrollable">
            <div class="wrapper">

                <div class="text-center m-b m-t">
                    <a href="#" class="thumb-lg">
                        <img src="{{ $user->avatar_image }}" class="img-circle">
                    </a>
                    <div>
                        <div class="h3 m-t-xs m-b-xs">{{ $user->name }}</div>
                    </div>                
                </div>

                <div class="panel wrapper">
                    <div class="row text-center">
                        <div class="col-xs-6">
                            <a href="#">
                                <span class="m-b-xs h4 block">{{ (isset($meta->following)) ? $meta->followers : 0 }}</span>
                                <small class="text-muted">Followers</small>
                            </a>
                        </div>
                        <div class="col-xs-6">
                            <a href="#">
                                <span class="m-b-xs h4 block">{{ (isset($meta->following)) ? $meta->following : 0 }}</span>
                                <small class="text-muted">Following</small>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="btn-group btn-group-justified m-b">
                    @if(Auth::user())
                    @if(Auth::user()->id !== $user->id)
                    <a class="btn btn-success btn-rounded {{ ($user->is_followed_by( Auth::user()->id )) ? "active" : "" }}" data-toggle="button" id="{{ ($user->is_followed_by( Auth::user()->id )) ? "unfollow" : "follow" }}">
                        <span class="text">
                            <i class="fa fa-eye"></i> Follow
                        </span>
                        <span class="text-active">
                            <i class="fa fa-eye"></i> Following
                        </span>
                    </a>
                    @endif
                    @endif
                </div>
                <div>
                    <small class="text-uc text-xs text-muted">User type</small>
                    <p>{{ $user_type }}</p>
                    @if(isset($meta->info) && "" !== json_decode($meta->info))
                    <small class="text-uc text-xs text-muted">Short info</small>
                    <p>
                        {!! (isset($meta->info)) ? json_decode($meta->info) : "" !!}
                    </p>
                    @endif

                    <div class="line"></div>
                    <!-- If some social networks exists, render the section -->
                    @if( (isset($meta->fb) && $meta->fb !== "") 
                    || (isset($meta->tw) &&  "" !== $meta->tw) 
                    || (isset($meta->gp) && $meta->gp !== ""))
                    <small class="text-uc text-xs text-muted">Connection</small>
                    <p class="m-t-sm">
                        @if(isset($meta->fb) && $meta->fb !== "")<a href="{{ (isset($meta->fb)) ? $meta->fb : "" }}" class="btn btn-rounded btn-twitter btn-icon"><i class="fa fa-facebook"></i></a>@endif
                        @if(isset($meta->tw) && $meta->tw !== "")<a href="{{ (isset($meta->tw)) ? $meta->tw : "" }}" class="btn btn-rounded btn-facebook btn-icon"><i class="fa fa-twitter"></i></a>@endif
                        @if(isset($meta->gp) && $meta->gp !== "")<a href="{{ (isset($meta->gp)) ? $meta->gp : "" }}" class="btn btn-rounded btn-gplus btn-icon"><i class="fa fa-google-plus"></i></a>@endif
                    </p>
                    @else
                    <p>User has no connections</p>
                    @endif
                </div>
            </div>
        </section>
    </section>
</aside>