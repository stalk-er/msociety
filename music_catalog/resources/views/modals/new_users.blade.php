<h3 class="font-thin pos-rlt m-l m-b">Hey, checkout your new friends</h3>

<div class="row row-sm">
    @if($new_users)
    <?php $i = 0; ?>
    @foreach($new_users as $new_user)
    <div class="item pos-rlt listen_banner">
        <a href="{{ url('/') }}/user/{{ $new_user->id }}" 
           data-bjax="" data-replace="{{ url('/') }}/user/{{ $new_user->id }}" 
           data-url="{{ url('/') }}/user/{{ $new_user->id }}" 
           data-target=".to_put" data-el="#bjax-target" 
           @if($i % 2 == 0)
           class="item-overlay active opacity wrapper-md font-xs text-right"
           @else
           class="item-overlay active opacity wrapper-md font-xs text-left"
           @endif
           >
           <span class="block h3 font-bold  
              @if($i%2 == 0)
              text-warning
              @else
              text-info
              @endif
              ">{{ $new_user->name }}</span>
            <span class="text-muted">Recently registered profile, give him a hand</span>
            <span class="bottom wrapper-md block"><i class="icon-arrow-right i-lg pull-left"></i></span>
        </a>
        <a href="#">
            <img class="img-full" src="{{ $new_user->avatar_image }}" alt="{{ $new_user->name }}">
        </a>
    </div>
    <?php $i++; ?>
    @endforeach
    @endif
</div>
