<aside class="bg-black dk nav-xs aside hidden-print" id="nav">
    <section class="vbox">
        <section class="w-f-md scrollable">
            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">

                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                    <ul class="nav bg clearfix">
                        <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted"></li>
                        <li>
                            <a href="/" data-bjax="" data-url="/" data-replace="/" data-target=".to_put" data-el="#bjax-target">
                                <i class="icon-disc icon text-success"></i>
                                <span class="font-bold">What's new</span>
                            </a>
                        </li>
                        <li>
                            <a href="/genres" data-bjax="" data-replace="/genres" data-url="/genres" data-target=".to_put" data-el="#bjax-target">
                                <i class="icon-music-tone-alt icon text-info"></i>
                                <span class="font-bold">Genres</span>
                            </a>
                        </li>

                        @if(Auth::user())
                        @if(Auth::user()->hierarchy_type()->name !== "Fan" )
                        <!--                        <li>
                                                    <a href="/events" data-bjax="" data-replace="/events" data-url="/events" data-target=".to_put" data-el="#bjax-target">
                                                        <i class="icon-drawer icon text-primary-lter"></i>
                                                        <b class="badge bg-primary pull-right">6</b>
                                                        <span class="font-bold">Events</span>
                                                    </a>
                                                </li>-->
                        @endif
                        @endif
                        <li>
                            <a href="/listen" data-bjax data-url="/listen" data-replace="/listen" data-target=".to_put" data-el="#bjax-target">
                                <i class="icon-list icon  text-info-dker"></i>
                                <span class="font-bold">Listen</span>
                            </a>
                        </li>
                        <li class="m-b hidden-nav-xs"></li>

                        @if(Auth::user())
                        @if(Auth::user()->hierarchy_type()->name !== "Fan" )
                        <li>
                            <a href="/uploads" data-bjax="" data-url="/uploads" data-replace="/uploads" data-target=".to_put" data-el="#bjax-target">
                                <i class="fa fa-upload text-darker"></i>
                                <span class="font-bold">Uploads</span>
                            </a>
                        </li>
                        @endif
                        @endif

                        <li>
                            <a href="/history" data-bjax="" data-url="/history" data-replace="/history" data-target=".to_put" data-el="#bjax-target">
                                <i class="icon-flag icon  text-flag-dker"></i>
                                <span class="font-bold">History</span>
                            </a>
                        </li>
                        <li class="m-b hidden-nav-xs"></li>
                    </ul>

                    @if(Auth::user())
                    <ul class="nav text-sm playlists">
                        <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
                            <span class="pull-right">
                                <a style="cursor: pointer;">
                                    <i class="icon-plus i-lg create_playlist"
                                       data-toggle="popover"
                                       data-container="#content"
                                       data-html="true"
                                       data-placement="top"
                                       data-content='<div style="height:auto; width: auto; z-index: 9999; padding: 10px;">
                                       <form role="form">
                                       <div class="form-group">
                                       <label class="sr-only" for="playlist_name">Name</label>
                                       <input type="text" class="form-control" id="playlist_name" name="name" placeholder="Name">
                                       </div>
                                       <div class="form-group">
                                       <label class="sr-only" for="playlist_description">Description</label>
                                       <input type="text" class="form-control" id="playlist_description" name="description" placeholder="Description">
                                       </div>
                                       <button type="button" class="btn btn-success" id="createPlaylist">Create</button>
                                       </form>
                                       </div>'
                                       title=""
                                       data-animation="true"
                                       data-original-title='<button type="button" class="close pull-right" data-dismiss="popover"> &times; </button>Create a playlist'>
                                    </i>
                                </a>
                            </span>
                            Playlists
                        </li>

                        @foreach($playlists as $playlist)
                        <li data-playlist="{{ $playlist->id }}">
                            <a style="cursor: pointer;"
                               data-toggle="popover"
                               data-container="#content"
                               data-html="true"
                               data-placement="right"
                               data-content='
                               <div class="load_playlist_popover" data-playlist="{{ $playlist->id }}" 
                               style="height:auto; width: auto; z-index: 9999; padding: 10px;">
                               <h4 class="text-center">Are you sure you want to load this playlist now ?</h4>
                               <button type="button" class="btn btn-success btn-md loadNow">Load Playlist</button>
                               <button type="button" class="btn btn-danger btn-md deleteNow">Delete Playlist</button>
                               </div>'
                               title="{{ $playlist->name }}"
                               data-animation="true"
                               data-original-title='<button type="button" class="close pull-right" data-dismiss="popover"> &times; </button>Load Playlist'>
                                <i class="icon-playlist icon text-success-lter"></i>
                                <b class="badge bg-success dker pull-right num_of_songs">{{ count($playlist->songs()) }}</b>
                                <span>{{ $playlist->name }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @endif
                </nav>
                <!-- / nav -->

            </div>
        </section>

        @if(Auth::user())
        <footer class="footer hidden-xs no-padder text-center-nav-xs">
            <div class="bg hidden-xs ">
                <div class="dropdown dropup wrapper-sm clearfix">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="thumb-sm avatar pull-left m-l-xs">
                            <img src="{{ Auth::user()->avatar_image }}" class="dker avatar_image" alt="{{ Auth::user()->name }}">
                            <i class="on b-black"></i>
                        </span>
                        <span class="hidden-nav-xs clear">
                            <span class="block m-l">
                                <strong class="font-bold text-lt">{{ Auth::user()->name }}</strong>
                                <b class="caret"></b>
                            </span>
                            <span class="text-muted text-xs block m-l">{{ Auth::user()->hierarchy_type()->name }}</span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight aside text-left">
                        <li>
                            <span class="arrow bottom hidden-nav-xs"></span>
                            <a href="{{ url("/profile-settings") }}" data-bjax="" data-replace="{{ url("/profile-settings") }}" data-url="{{ url("/profile-settings") }}" data-target=".to_put" data-el="#bjax-target">Settings</a>
                        </li>
                        <li>
                            <a href="{{ url("user") . "/".Auth::user()->id }}" data-bjax="" data-replace="{{ url("user") . "/".Auth::user()->id }}" data-url="{{ url("user") . "/".Auth::user()->id }}" data-target=".to_put" data-el="#bjax-target">Profile</a>
                        </li>
                        <!--                        <li>
                                                    <a href="#">
                                                        <span class="badge bg-danger pull-right">3</span>
                                                        Notifications
                                                    </a>
                                                </li>-->
                        <li class="divider"></li>
                        <li>
                            <a href="{{ url("/logout") }}"s>Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </footer>
        @endif
    </section>
</aside>
<!-- /.aside -->
