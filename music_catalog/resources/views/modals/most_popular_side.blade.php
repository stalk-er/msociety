<section class="col-sm-4 no-padder bg">
    <section class="vbox">
        <section class="scrollable hover">
            <h2 class="font-thin m-b genre_main_title" style="margin-left: 15px;">Most listened</h2>
            <ul class="list-group list-group-lg no-bg auto m-b-none m-t-n-xxs most_pop">
                @foreach($most_popular as $popular)
                <li class="item-{{ $popular->id }} list-group-item clearfix song listen" @if(Auth::user()) data-played="false" @endif>
                    <input type="hidden" id="song_id" value="{{ $popular->id }}" />
                    <input type="hidden" name="song_author" value="{{ $popular->author }}" /> <!-- Song preview will loading the image from here -->
                    <input type="hidden" name="song_cover" value="{{ $popular->cover }}" /> <!-- Song preview will loading the image from here -->

                    <!-- The song meta -->
                    <span hidden class="song_meta">
                        <input type="hidden" value="{{ $popular->file }}" name="mp3" />
                        <input type="hidden" value="{{ $popular->author }}" name="artist" />
                        <input type="hidden" value="{{ $popular->name }}" name="title" />
                        <input type="hidden" value="{{ $popular->cover }}" name="poster" />
                        <input type="hidden" value="{{ $popular->url }}" name="url" />
                    </span>

                    <!-- Play/Pause -->
                    <a href="#" class="jp-play-me pull-right m-t-sm m-l text-md">
                        <i class="icon-control-play text"></i>
                        <i class="icon-control-pause text-active"></i>
                    </a>

                    <div class="pull-right m-t-sm m-l text-md">
                        <a href="{{ url("/") . "/songs/".$popular->id . "/download"  }}" class="m-r-sm" title="Download"><i class="fa fa-download"></i></a>
                        @if(Auth::user())
                        <a href="#" class="m-r-sm pull-right {{ (Auth::user()->likes($popular)) ? 'active' : "" }} "  data-toggle="class">
                            <i class="fa fa-heart-o text"></i>
                            <i class="fa fa-heart text-active text-danger"></i>
                        </a>
                        <a href="#" class="m-r-sm">
                            <i class="fa fa-plus-circle add_to_playlist"
                               data-toggle="popover"
                               data-container="#content"
                               data-html="true"
                               data-placement="bottom"
                               tabindex="0"
                               data-content='
                               <ul class="dropdown-menu pos-stc inline" role="menu">
                               @if(count(Auth::user()->playlists()->get()) <= 0)
                               <li><a>No playlists found.</a></li>
                               @else @foreach(Auth::user()->playlists()->get() as $playlist)
                               <li><input type="hidden" id="song_id" value="{{ $popular->id }}" />
                               @if( $playlist->contains($popular) )<input type="hidden" name="in_playlist" />@endif
                               <a tabindex="-1" href="#" @if( $playlist->contains($popular) ) id="removeFromPlaylist" @else id="addToPlaylist" @endif data-playlist-id="{{ $playlist->id }}">{{ $playlist->name }}
                               @if( $playlist->contains($popular) )<i class="fa fa-check pull-right"></i>@endif</a></li>@endforeach @endif</ul>'
                               title=""
                               data-animation="true"
                               data-original-title='Add to playlist'
                               ></i>
                        </a>
                        @endif
                    </div>
                    <a href="#" class="pull-left thumb-sm m-r">
                        <img src="{{ $popular->cover }}" alt="{{ $popular->name }}">
                    </a>
                    <a class="clear">
                        <span class="block text-ellipsis">{{ $popular->name }}</span>
                        <small class="text">by {{ $popular->author }}</small>
                    </a>
                </li>

                @endforeach
            </ul>
        </section>
    </section>
</section>
