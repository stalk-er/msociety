<!-- side content -->
<aside class="aside-md bg-light dk" id="sidebar" style="height: 500px;">
    <section class="vbox animated fadeInRight">
        <section class="w-f-md scrollable hover">
            <h4 class="font-thin m-l-md m-t">My followings</h4>
            <ul class="list-group no-bg no-borders auto m-t-n-xxs" id="members"> <!-- past => people -->
                @if(count($users) <= 0)
                <li class="list-group-item">No followings exists</li>
                @else
                @foreach($users as $user)
                <li class="list-group-item"  
                    data-toggle="popover" 
                    data-container="#members" 
                    data-html="true"
                    data-placement="bottom"
                    tabindex="0"
                    title=""
                    data-animation="true"
                    data-original-title='Currently not listening to anything.'>
                    <input type="hidden" value="{{ $user->id }}" class="uid" />
                    <input type="hidden" value="{{ $user->name }}" class="uname" />
                    <span class="pull-left thumb-xs m-t-xs avatar m-l-xs m-r-sm">
                        <a>
                            <img src="{{ $user->avatar_image }}" alt="" class="img-circle">
                        </a>
                        <i class="off b-light right sm"></i>
                    </span>
                    <div class="clear">
                        <div><a class="member_name" href="/user/{{ $user->id }}" data-bjax="" data-url="/user/{{ $user->id }}" data-replace="/user/{{ $user->id }}" data-target=".to_put" data-el="#bjax-target">{{ $user->name }}</a></div>
                        <small class="text-muted"><a target="_top" href="mailto:{{ $user->email }}?Subject=I%20like%20your%20music">{{ $user->email }}</a></small>
                    </div>
                </li>
                @endforeach
                @endif
            </ul>
        </section>
        <footer class="footer footer-md bg-black">
            <form class="" role="search">
                <div class="form-group clearfix m-b-none">
                    <div class="input-group m-t m-b">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-sm bg-empty text-muted btn-icon"><i class="fa fa-search"></i></button>
                        </span>
                        <input type="text" class="form-control input-sm text-white bg-empty b-b b-dark no-border" placeholder="Search members">
                    </div>
                </div>
            </form>
        </footer>
    </section>              
</aside>
<!-- / side content -->