<!DOCTYPE html>
<html lang="en" class="app">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="token" name="token" content="{{ csrf_token() }}">
        <title>Music Application</title>
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.structure.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.theme.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('js/jPlayer/jplayer.flat.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/simple-line-icons.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/font.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('js/datepicker/datepicker.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('js/slider/slider.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('js/chosen/chosen.css') }}" type="text/css" />
        <!--[if lt IE 9]>
            <script src="{{ asset('js/ie/html5shiv.js') }}"></script>
            <script src="{{ asset('js/ie/respond.min.js') }}"></script>
            <script src="{{ asset('js/ie/excanvas.js') }}"></script>
        <![endif]-->
        <style>
            .panel {
                z-index: 1;
            }
            table {
                position: relative;
                z-index: 6;
            }
            .col-md-3.song, .item.song {
                min-height: 300px;
            }
            .ui-menu.ui-widget {
                z-index: 9999;
            }
            .highlight {
                font-weight: bold;
            }
            .activity-history .panel {
                position: relative;
                z-index: 1;
            }
            .scrollable {
                padding-bottom: 40px;
            }
            footer.footer {
                position: fixed;
                bottom: 0;
                top: auto;
                z-index: 20;
            }
            .popover-content {
                padding: 0;
            }

            .popover-content .dropdown-menu.pos-stc.inline {
                height: 200px;
                overflow-y: scroll;
                overflow-x: hidden;
            }

            tr td .fa.fa-play-circle.i-2x {
                display: none;
            }

            tr.history_item td:hover .fa.fa-play-circle.i-2x {
                display: inline-block;
                cursor: pointer;
            }

            .history-item .fname {
                display: inline-block;
            }
            @media (max-width: 767px) {
                .login-logout, .upload-button-header {
                    text-align: center;
                }
                .upload-button-header {
                    display: block;
                }
                .login-logout a, .upload-button-header a {
                    margin: 20px 10px;
                }
            }
            @media (min-width: 768px) {
                .login-logout a, .upload-button-header a {
                    display: inline-block;
                    margin: 0 10px;
                    margin-top: 12px;
                }
                .upload-button-header {
                    display: inline-block;
                }
            }

            .ui-menu-item-wrapper.ui-state-active {
                width: 100%;
            }
        </style>

        <!-- JavaScripts -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
        <!-- <script src="{{ asset('js/custom.js') }}"></script>-->

    </head>
    <input type="hidden" id="is_logged_js" value="{{ (Auth::user()) ? 1 : 0 }}" />
    <body>
        <section class="vbox">
            <header class="bg-white-only header header-md navbar navbar-fixed-top-xs">
                <div class="navbar-header aside bg-info nav-xs">
                    <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen,open" data-target="#nav,html">
                        <i class="icon-list"></i>
                    </a>
                    <a href="{{ url('/') }}" class="navbar-brand text-lt">
                        <i class="icon-earphones"></i>
                        <img src="images/logo.png" alt="." class="hide">
                        <span class="hidden-nav-xs m-l-sm">MSociety</span>
                    </a>
                    <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".user">
                        <i class="icon-settings"></i>
                    </a>
                </div>
                <ul class="nav navbar-nav hidden-xs">
                    <li>
                        <a href="#nav,.navbar-header" data-toggle="class:nav-xs,nav-xs" class="text-muted">
                            <i class="fa fa-indent text"></i>
                            <i class="fa fa-dedent text-active"></i>
                        </a>
                    </li>
                </ul>
                <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-sm bg-white btn-icon rounded"><i class="fa fa-search"></i></button>
                            </span>
                            <input type="search" id="primary-search" class="form-control input-sm no-border rounded" placeholder="Search songs, albums...">
                        </div>
                    </div>
                </form>

                @if(session()->has('success'))
                <div class="alert alert-success alert-block" style="position: fixed; width: 100%; z-index: 9;">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <h4><i class="fa fa-bell-alt"></i>Success!</h4>
                    <p>{{ session()->get('success') }}</p>
                </div>
                @endif

                @if(Auth::user())
                @if(Auth::user()->hierarchy_type()->name !== "Fan" )
                <div class="upload-button-header">
                    <a href="#upload_file" class="btn btn-s-md btn-info" id="upload_now" data-toggle="modal">Upload</a>
                </div>
                @endif
                <div class="navbar-right " style="height: 60px;">
                    <ul class="nav navbar-nav m-n hidden-xs nav-user user" id="notifications">
                        <li class="hidden-xs">
                            <a href="#" class="dropdown-toggle lt" data-toggle="dropdown">
                                <i class="icon-bell"></i>
                                <span class="badge badge-sm up bg-danger count notification_count">0</span>
                            </a>
                            <section class="dropdown-menu aside-xl animated fadeInUp">
                                <section class="panel bg-white">
                                    <div class="panel-heading b-light bg-light">
                                        <strong>You have <span class="count">0</span> notifications</strong>
                                    </div>
                                    <div class="list-group list-group-alt notification_list">
                                        <!--                                        <a href="#" class="media list-group-item">
                                                                                    <span class="pull-left thumb-sm">
                                                                                        <img src="images/a0.png" alt="..." class="img-circle">
                                                                                    </span>
                                                                                    <span class="media-body block m-b-none">
                                                                                        Use awesome animate.css<br>
                                                                                        <small class="text-muted">10 minutes ago</small>
                                                                                    </span>
                                                                                </a>-->
                                    </div>
                                    <div class="panel-footer text-sm">
                                        <a href="#" class="pull-right"><i class="fa fa-cog"></i></a>
                                        <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                                    </div>
                                </section>
                            </section>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle bg clear" data-toggle="dropdown">
                                <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                                    <img class="avatar_image" src="{{ Auth::user()->avatar_image }}" alt="...">
                                </span>
                                {{ Auth::user()->name }} <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight">
                                <li>
                                    <span class="arrow top"></span>
                                    <a href="{{ url("/profile-settings") }}" data-bjax="" data-replace="{{ url("/profile-settings") }}" data-url="{{ url("/profile-settings") }}" data-target=".to_put" data-el="#bjax-target">Settings</a>
                                </li>
                                <li>
                                    <a href="{{ url("user") . "/".Auth::user()->id }}" data-bjax="" data-replace="{{ url("user") . "/".Auth::user()->id }}" data-url="{{ url("user") . "/".Auth::user()->id }}" data-target=".to_put" data-el="#bjax-target">Profile</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ url("/logout") }}">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                @else
                <div class="login-logout">
                    <a href="{{ url("/login") }}" class="btn btn-s-md btn-success">Login</a>
                    <a href="{{ url("/register") }}" class="btn btn-s-md btn-warning">Register</a>
                </div>
                @endif
            </header>
            @yield('content')

            @include('modals.upload')
        </section>


        <!-- Bootstrap -->
        <script src="{{ asset('js/bootstrap.js') }}"></script>

        <!-- App -->
        <script src="{{ asset('js/app.js') }}"></script>

        <!-- Cookies JS -->
        <script src="{{ asset('js/js.cookie.js') }}"></script>
        <script src="{{ asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('js/app.plugin.js') }}"></script>
        <script src="{{ asset('/js/charts/sparkline/jquery.sparkline.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('js/jPlayer/jquery.jplayer.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jPlayer/add-on/jplayer.playlist.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jPlayer/demo.js') }}"></script>

        <script src="{{ asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>

        <!-- datepicker -->
        <script src="{{ asset('js/datepicker/bootstrap-datepicker.js') }}"></script>

        <!-- Clipboard -->
        <script src="{{ asset('js/clipboard/clipboard.min.js') }}"></script>

        <!-- slider -->
        <script src="{{ asset('js/slider/bootstrap-slider.js') }}"></script>

        <!-- wysiwyg -->
        <script src="{{ asset('js/wysiwyg/jquery.hotkeys.js') }}"></script>
        <script src="{{ asset('js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
        <script src="{{ asset('js/wysiwyg/demo.js') }}"></script>


        <script src="{{ asset('js/chosen/chosen.jquery.min.js') }}"></script>
<!--        <script src="{{ asset('js/chosen/chosen.jquery-extended.min.js') }}"></script>-->
        <script src="{{ asset('js/moment/moment.js') }}"></script>
        <script src="{{ asset('js/parsley/parsley.min.js') }}"></script>
        <script src="{{ asset('js/wizard/jquery.bootstrap.wizard.js') }}"></script>

        <!-- {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}-->

        <script src="{{ asset('js/NotificationManager.js') }}"></script>
        <script src="{{ asset('js/Requester.js') }}"></script>
        <script src="{{ asset('js/SongManager.js') }}"></script>
        <script src="{{ asset('js/PlaylistManager.js') }}"></script>
        <script src="{{ asset('js/HistoryManager.js') }}"></script>
        <script src="{{ asset('js/Application.js') }}"></script>
        <script src="{{ asset('js/MessageManager.js') }}"></script>
        <script src="{{ asset('js/Player.js') }}"></script>
        <script src="{{ asset('js/User.js') }}"></script>

        <script src="//cdn.socket.io/socket.io-1.3.4.js"></script>

        <script type="text/javascript">
var Application = new Application();
Application.start();


var NotificationManager = new NotificationManager();

var SongManager = new SongManager();
SongManager.init();

var PlaylistManager = new PlaylistManager();
PlaylistManager.init();

var HistoryManager = new HistoryManager();

$(document).ready(loadSockets);
function loadSockets() {

    // Connect to our node/websockets server
    var PORT = 7678;
    var POLLING_INTERVAL = 1000;

    var socket = io.connect(window.location.origin + ':' + PORT, {
        'multiplex': false,
        'auto connect': true,
        'max reconnection attempts': 2,
        'forceNew': true
    });


    // Connect
    socket.on('connect', function () {

        // Check online members
        socket.emit('members', {
            user_id: parseInt($("#current_user").val()),
            members: get_members()
        });

        socket.on('user friends', function (friends) {
            $("#members").find(".b-light.right.sm").removeClass("on").addClass("off");
            $("#members").find(".b-light.right.sm").removeClass("on");
            for (var i in friends.online_members) {
                $("#members").find("input[value='" + friends.online_members[i] + "']")
                        .closest(".list-group-item").find(".b-light.right.sm").removeClass("off").addClass("on");
            }
        });

        // Current song
        $("input[type='hidden']#emit_current_song").bind('change', function () {
            var current_playing = JSON.parse($(this).val());

            if (typeof current_playing !== "undefined") {
                socket.emit("currently playing", current_playing);
            }
        });

        socket.on('friend is playing', function (user_played) {

            var current_member_name = $("ul#members")
                    .find("input[value='" + user_played.user_id + "'].uid")
                    .closest("li[data-toggle='popover']")
                    .find(".uname").val();

            var original_title = "<strong>" + current_member_name + "</strong> is currently listening to <strong>" + user_played.playing + "</strong>";

            $("ul#members")
                    .find("input[value='" + user_played.user_id + "'].uid")
                    .closest("li[data-toggle='popover']")
                    .attr("data-original-title", original_title)
                    .popover('show');

            setTimeout(function () {
                $("ul#members")
                        .find("input[value='" + user_played.user_id + "'].uid")
                        .closest("li[data-toggle='popover']")
                        .popover('hide');
            }, 5 * 1000);
        });

        function get_existing_notifications() {
            var _notifications = [];
            $('.notification_list').find('.media.list-group-item').each(function () {
                _notifications.push(Number($(this).find('#notification_id').val()));
            });
            return _notifications;
        }

        $(window).blur(function () {

            // Pull latest notifications
            window.clearInterval(window.pooling);
        });


        $(window).focus(function () {
            //your code

            $loongPooling();
        });

        var $loongPooling = function () {

            window.pooling = setInterval(function () {
                // Notifications handle
                socket.emit('anything new?', {
                    user_id: Number($("#current_user").val()),
                    notifications: get_existing_notifications()
                });
            }, POLLING_INTERVAL);
        };
        $loongPooling();
        // Rendering real time notifications
        socket.on('notifications', function (latest) {

            console.log("notifications");
            console.log(latest);

            var noe = latest.length;
            $("#notifications").find(".notification_count").text(noe + Number($("#notifications").find(".notification_count").text()));

            if (null !== latest) {
                for (var i in latest) {
                    var html = '<a href="#" data-seen="not-yet" class="media list-group-item" onclick="Application.seeNotification(this)">' +
                            '<input type="hidden" value="' + latest[i].id + '" id="notification_id" />' +
                            '<span class="pull-left thumb-sm"> ' +
                            '<img src="images/a0.png" alt="..." class="img-circle">' +
                            '</span>' +
                            '<span class="media-body block m-b-none">' +
                            latest[i].activity_content + '<br>' +
                            '<small class="text-muted">10 minutes ago</small>' +
                            '</span>' +
                            '</a>';
                    $(".notification_list").append($(html));
                }
            }
        });
    });

    // Get the current user friends
    function get_members() {
        var members = [];
        $("#members").find("li").each(function () {
            var member_id = parseInt($(this).find("input[type='hidden'].uid").val());
            if (isNaN(member_id))
                return;
            members.push(member_id);
        });
        return members;
    }
}
        </script>

        @if(Auth::user())
        <input type="hidden" id="current_user" value="{{ Auth::user()->id }}" />
        <input type="hidden" id="emit_current_song" value="" />
        <input type="hidden" id="anythingNew" onchange="" />
        @endif
    </body>
</html>
