@extends('layouts.app')

@section('content')
<section id="content" class="m-t-lg wrapper-md animated fadeInDown scrollable">
    <div class="container aside-xl">
        <a class="navbar-brand block" href="{{url('/')}}"><span class="h1 font-bold">MSociety</span></a>
        <section class="m-b-lg">
            <header class="wrapper text-center">
                <strong>Sign up to find interesting thing</strong>
            </header>

            <form class="reg" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input placeholder="Name" name="name" value="{{ old('name') }}" class="form-control rounded input-lg text-center no-border">
                    @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" placeholder="Email" name="email" value="{{ old('email') }}" class="form-control rounded input-lg text-center no-border">
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" name="password" placeholder="Password" class="form-control rounded input-lg text-center no-border">
                    @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <input id="password-confirm" type="password" placeholder="Confirm password" class="form-control rounded input-lg text-center no-border" name="password_confirmation" />
                    @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="form-group col-sm-12 col-md-4">
                    <select name="type" id="user_role_choose" class="form-control m-b">
                        @foreach($user_types as $type)
                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="checkbox i-checks m-b">
                        <label class="m-l">
                            <input type="checkbox" checked="" required="required"><i></i> Agree the <a href="#">terms and policy</a>
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-lg btn-warning lt b-white b-2x btn-block btn-rounded"><i class="icon-arrow-right pull-right"></i><span class="m-r-n-lg">Sign up</span></button>
                <div class="line line-dashed"></div>
                <p class="text-muted text-center"><small>Already have an account?</small></p>
                <a href="signin.html" class="btn btn-lg btn-info btn-block btn-rounded">Sign in</a>
            </form>
        </section>

    </div>
    <div class="row">
        <div class="col-sm-12">

            <form style="display: none;" id="wizardform" method="get" action="">
                <h2 style="color: #4cb6cb;">Please answer the following questions to verify that you are a musician.</h2>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs font-bold">
                            <li class="active"><a href="#step1" data-toggle="tab">Question 1</a></li>
                            <li><a href="#step2" data-toggle="tab">Question 2</a></li>
                            <li><a href="#step3" data-toggle="tab">Question 3</a></li>
                        </ul>
                    </div>

                    <div class="panel-body">

                        <div class="line line-lg"></div>
                        <div class="tab-content">
                            <div class="line line-lg"></div>
                            <div class="tab-pane active" id="step1"> 
                                <h3>Which of these notes is <strong>G</strong></h3>
                                <div class="line line-lg"></div>

                                <div class="form form-group">
                                    <div class="form-group">
                                        <label><input type="radio" value="1" name="which_notes"/> <img src="/register_quiz/q1a1.png" width="120" /></label>
                                    </div>
                                    <div class="form-group">
                                        <label><input type="radio" value="2" name="which_notes"/> <img src="/register_quiz/q1a2.png" width="120"/></label>
                                    </div>
                                    <div class="form-group">
                                        <label><input type="radio" value="3" name="which_notes" /> <img src="/register_quiz/q1a3.png"width="120" /></label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="step2">
                                <h3>Which minor scale has a key signature with four sharps ?</h3>
                                <div class="line line-lg"></div>

                                <div class="form-group">
                                    <div class="radio">
                                        <label><input type="radio" value="G#" name="minor_scale" /><h4>G sharp</h4></label>
                                    </div>
                                    <div class="radio">
                                        <label><input class="" type="radio" value="C#" name="minor_scale" /><h4>C sharp</h4></label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" value="B#" name="minor_scale" /><h4>B sharp</h4></label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="step3">
                                <h3>What is the name of this interval ?</h3>
                                <div class="line line-lg"></div>
                                <div class="form-group"><img src="/register_quiz/q3.png" /></div>
                                <div class="line line-lg"></div>
                                <div class="form-group">
                                    <div class="radio">
                                        <label><input type="radio" value="D5" name="intervals"/><h4>Diminished 5th</h4></label>
                                    </div>
                                    <div class="radio">
                                        <label><input class="" type="radio" name="intervals" value="M5"/><h4>Minor 5th</h4></label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="intervals" value="A5"/><h4>Augmented 5th</h4></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button id="submit_quiz" class="btn btn-lg btn-primary">Ready ?</button>
            </form>
        </div>
    </div>
</section>



<script>
//    $(document).ready(function (e) {
//
//        $("#user_role_choose").change(function () {
//            /** 
//             * User roles are:
//             * 
//             * 1 - Fan
//             * 2 - Uploader
//             * 
//             * */
//            var user_type = parseInt($(this).val());
//            if (user_type == 1) { // Fan
//                console.log("You are just a regular user. You can proceed.");
//            }
//            else { // Uploader
//
//                $("#wizardform").fadeIn();
//                $("form.reg").fadeOut();
//            }
//        });
//
//        var q1_answer = 2;
//        var q2_answer = "C#";
//        var q3_answer = "A5";
//
//
//
//
//        $("#submit_quiz").click(function (e) {
//            e.preventDefault();
//
//            var q1a = parseInt($('input[name="which_notes"]:checked', '#wizardform').val());
//            var q2a = $('input[name="minor_scale"]:checked', '#wizardform').val();
//            var q3a = $('input[name="intervals"]:checked', '#wizardform').val();
//
//            console.log(q1a);
//            console.log(q2a);
//            console.log(q3a);
//            if (q1a == q1_answer && q2a == q2_answer && q3a == q3_answer) {
//                action_msg("alert-success", "Congratulations! You can now Sign up.");
//
//                $("#wizardform").fadeOut();
//                $("form.reg").fadeIn();
//                $("button[type='submit']").fadeIn();
//            }
//
//        });
//
//    });
</script>
@endsection
